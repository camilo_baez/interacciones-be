﻿/*

DROP TABLE departamento;
DROP TABLE distrito;
DROP TABLE localidad;

*/


CREATE TABLE departamento(
	geom geometry,
	dpto text,
	dpto_desc text,
	cartodb_id text,
	created_at text, 
	updated_at text
);
COPY departamento FROM '/home/sortiz/Downloads/paraguay_2012_departamentos.csv' WITH CSV HEADER DELIMITER AS ',';

CREATE TABLE distrito(
	geom geometry,
	dpto text,
	distrito text,
	dpto_desc text,
	dist_desc text,
	codigo text,
	cartodb_id text,
	created_at text,
	updated_at text
);
COPY distrito FROM '/home/sortiz/Downloads/paraguay_2012_distritos.csv' WITH CSV HEADER DELIMITER AS ',';

CREATE TABLE localidad(
	geom geometry,
	objectid text,
	dpto text,
	distrito text,
	dpto_desc text,
	dist_desc text,
	area text,
	bar_loc text,
	barlo_desc text,
	cant_viv text,
	codigo text,
	superf text,
	cartodb_id text,
	created_at text,
	updated_at text
);
COPY localidad FROM '/home/sortiz/Downloads/paraguay_2012_barrios_y_localidades.csv' WITH CSV HEADER DELIMITER AS ',';


--SELECTS
COPY (
	SELECT 'INSERT INTO departamento(id,codigo,nombre,pais_id) VALUES (' || cartodb_id || ',''' || dpto || ''',' || quote_literal(dpto_desc) || ',1);'
	FROM departamento
	ORDER BY dpto
) TO '/tmp/insert_departamento.txt';


COPY (
	SELECT 'INSERT INTO distrito(id,codigo,nombre,departamento_id) VALUES (' || di.cartodb_id || ',''' || di.codigo || ''',' || quote_literal(di.dist_desc) || ',' || de.cartodb_id || ');'
	FROM distrito di JOIN departamento de ON di.dpto=de.dpto
	ORDER BY di.dpto, di.distrito
) TO '/tmp/insert_distrito.txt';


COPY (
	SELECT 'INSERT INTO localidad(id,codigo,nombre,zona,distrito_id) VALUES (' || l.cartodb_id || ',''' || l.codigo || ''',' || quote_literal(l.barlo_desc) || ',''' || l.area || ''',' || d.cartodb_id || ');'
	FROM localidad l JOIN distrito d ON (l.dpto||l.distrito)=d.codigo
	ORDER BY l.dpto||l.distrito
) TO '/tmp/insert_localidad.txt';




select * from departamento limit 10;
select * from distrito limit 10;
select * from localidad order by cartodb_id::int;