﻿SELECT product_id AS id, name AS nombre, description AS descripcion
FROM product
ORDER BY name;

--SELECT MAX(process_type_id) FROM t_process_type;

--SELECT * FROM process_type;

SELECT tpt.process_type_id, tpt.name, tpt.description, tpt.product_id
FROM product p JOIN t_process_type tpt ON p.product_id=tpt.product_id
WHERE tpt.status='A'
ORDER BY tpt.name;


--SELECTS
COPY (
	SELECT 'INSERT INTO producto(id,nombre,descripcion) VALUES (' || product_id || ',''' || name || ''',''' || COALESCE(description,'') || ''');'
	FROM product
	ORDER BY name
) TO '/tmp/insert_producto.txt';

COPY (
	SELECT 'INSERT INTO tipo_producto(id,nombre,descripcion,producto_id) VALUES (' || tpt.process_type_id || ',''' || tpt.name || ''',''' || COALESCE(tpt.description,'') || ''',' || tpt.product_id || ');'
	FROM product p JOIN t_process_type tpt ON p.product_id=tpt.product_id
	WHERE tpt.status='A'
	ORDER BY p.name, tpt.name
) TO '/tmp/insert_tipo_producto.txt';