﻿--1
SET datestyle = "ISO, DMY";
COPY medicamento (codigo, nombre, via)
FROM '/home/camilo/Descargas/datos_iniciales_medicamentos.csv' DELIMITER ',' CSV  quote '"' header;
--FROM '/Users/admin/desarrollo/feli_tg/archivos/medicamentos.csv' DELIMITER ',' CSV  quote '"' header;

--2


CREATE TABLE public.medicamento_activo_temp
(
  codigo bigint,
  activo_1 character varying,
  activo_2 character varying,
  activo_3 character varying,
  activo_4 character varying,
  activo_5 character varying
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.medicamento
  OWNER TO postgres;

--3
SET datestyle = "ISO, DMY";
COPY medicamento_activo_temp (codigo, activo_1, activo_2, activo_3, activo_4, activo_5)
FROM '/home/camilo/Descargas/datos_iniciales_medicamentos_activos.csv' DELIMITER ',' CSV  quote '"' header;
--FROM '/Users/admin/desarrollo/feli_tg/archivos/medicamentos.csv' DELIMITER ',' CSV  quote '"' header;


--4
INSERT INTO medicamento_activo (medicamento, activo) select med.id, a1.id from medicamento_activo_temp mat
								join medicamento med on med.codigo = mat.codigo
								join activo a1 on a1.nombre = mat.activo_1;

INSERT INTO medicamento_activo (medicamento, activo) select med.id, a2.id from medicamento_activo_temp mat
								join medicamento med on med.codigo = mat.codigo
								join activo a2 on a2.nombre = mat.activo_2;

INSERT INTO medicamento_activo (medicamento, activo) select med.id, a3.id from medicamento_activo_temp mat
								join medicamento med on med.codigo = mat.codigo
								join activo a3 on a3.nombre = mat.activo_3;

INSERT INTO medicamento_activo (medicamento, activo) select med.id, a4.id from medicamento_activo_temp mat
								join medicamento med on med.codigo = mat.codigo
								join activo a4 on a4.nombre = mat.activo_4;

INSERT INTO medicamento_activo (medicamento, activo) select med.id, a5.id from medicamento_activo_temp mat
								join medicamento med on med.codigo = mat.codigo
								join activo a5 on a5.nombre = mat.activo_5;