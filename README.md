# Sistema de Interacciones - BackEnd

## Migración de base de datos

Flyway, mediante:

    mvn compile flyway:migrate

## Compilación y pruebas

Compilación del proyeco:

    mvn clean package

## Datasource

El datasource debe ser similar a:

```xml
<datasource jta="true" jndi-name="java:/interacciones" pool-name="interaccionesDS" enabled="true" use-ccm="true">
    <connection-url>jdbc:postgresql://localhost:5432/interacciones?stringtype=unspecified</connection-url>
    <driver-class>org.postgresql.Driver</driver-class>
    <driver>postgresql</driver>
    <security>
        <user-name>postgres</user-name>
        <password>postgres</password>
    </security>
    <validation>
        <valid-connection-checker class-name="org.jboss.jca.adapters.jdbc.extensions.postgres.PostgreSQLValidConnectionChecker"/>
        <background-validation>true</background-validation>
        <exception-sorter class-name="org.jboss.jca.adapters.jdbc.extensions.postgres.PostgreSQLExceptionSorter"/>
    </validation>
</datasource>

```

# Restauración de Backup
Para su caso particular, tal vez sea necesario ingresar el usuario con -U

```bash
pg_restore -U postgres -h localhost -p 5432 -d interacciones /tmp/interacciones_2017-04-20.sql
```
