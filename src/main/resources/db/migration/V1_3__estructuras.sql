
-- Table: public.medicamento

-- DROP TABLE public.medicamento;

CREATE TABLE public.medicamento
(
  id bigserial,
  codigo bigint unique,
  nombre character varying,
  descripcion character varying,
  tipo character varying,
  via character varying,
  controlado boolean default false,
  cantidad integer,
  unidad_medida character varying,
  concentracion character varying,
  unidad_medida_concentracion character varying,
  CONSTRAINT medicamento_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.medicamento
  OWNER TO postgres;


-- Table: public.activo

-- DROP TABLE public.activo;

CREATE TABLE public.activo
(
  id bigserial,
  nombre character varying unique,
  descripcion character varying,
  alto_riesgo boolean default false,
  CONSTRAINT activo_pkey PRIMARY KEY (id)
)
WITH (
OIDS=FALSE
);
ALTER TABLE public.activo
  OWNER TO postgres;

-- Table: public.severidad

-- DROP TABLE public.severidad;
CREATE TABLE public.severidad
(
  id bigserial,
  nivel integer,
  nombre character varying unique,
  descripcion character varying,
  CONSTRAINT severidad_pkey PRIMARY KEY (id)
)
WITH (
OIDS=FALSE
);
ALTER TABLE public.severidad
  OWNER TO postgres;

-- Table: public.paciente

-- DROP TABLE public.paciente;

CREATE TABLE public.paciente
(
  id bigserial,
  codigo bigint unique,
  nombre character varying,
  CONSTRAINT paciente_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.paciente
  OWNER TO postgres;

-- Table: public.interaccion

-- DROP TABLE public.interaccion;

CREATE TABLE public.interaccion
(
  id bigserial,
  activo_a bigint,
  activo_b bigint,
  severidad bigint,
  efecto character varying,
  CONSTRAINT interaccion_pkey PRIMARY KEY (id),
  CONSTRAINT interaccion_activo_a_fkey FOREIGN KEY (activo_a) REFERENCES public.activo (id) MATCH SIMPLE,
  CONSTRAINT interaccion_activo_b_fkey FOREIGN KEY (activo_b) REFERENCES public.activo (id) MATCH SIMPLE,
  CONSTRAINT interaccion_severidad_fkey FOREIGN KEY (severidad) REFERENCES public.severidad (id) MATCH SIMPLE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.interaccion
  OWNER TO postgres;

-- Table: public.salidas

-- DROP TABLE public.salidas;

CREATE TABLE public.salidas
(
  id bigserial,
  codigo bigint,
  paciente bigint,
  medicamento bigint,
  cantidad integer,
  fecha timestamp without time zone,
  CONSTRAINT salidas_pkey PRIMARY KEY (id),
  CONSTRAINT salidas_paciente_fkey    FOREIGN KEY (paciente) REFERENCES paciente(id),
  CONSTRAINT salidas_medicamento_fkey FOREIGN KEY (medicamento) REFERENCES medicamento(id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.salidas
  OWNER TO postgres;

-- Table: public.interaccion_paciente

-- DROP TABLE public.interaccion_paciente;

CREATE TABLE public.interaccion_paciente
(
  id bigserial,
  paciente bigint,
  interaccion bigint,
  fecha timestamp without time zone,
  CONSTRAINT interaccion_paciente_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.interaccion_paciente
  OWNER TO postgres;

-- Table: public.medicamento_activo

-- DROP TABLE public.medicamento_activo;

CREATE TABLE public.medicamento_activo
(
  id bigserial,
  medicamento bigint,
  activo bigint,
  CONSTRAINT medicamento_activo_pkey PRIMARY KEY (id)
)
WITH (
OIDS=FALSE
);
ALTER TABLE public.medicamento_activo
  OWNER TO postgres;
