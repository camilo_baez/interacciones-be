CREATE SEQUENCE usuario_id_sequence;
CREATE TABLE usuario (
	id bigint NOT NULL DEFAULT nextval('usuario_id_sequence'::regclass),
	nombre text NOT NULL,
	username text NOT NULL,
	contrasenha text NOT NULL,
	tipo_documento text NOT NULL,
	documento text NOT NULL,
	direccion text,
	correo text,
	telefono text,
	cambiar_contrasenha boolean DEFAULT false,
	configuraciones json DEFAULT '{}',
	estado text NOT NULL DEFAULT 'ACTIVO',
  	CONSTRAINT usuario_pkey PRIMARY KEY (id)
);

CREATE SEQUENCE rol_id_sequence;
CREATE TABLE rol (
	id bigint NOT NULL DEFAULT nextval('rol_id_sequence'::regclass),
	nombre  text NOT NULL,
	descripcion  text NOT NULL,
	CONSTRAINT rol_pkey PRIMARY KEY (id)
);

CREATE SEQUENCE permiso_id_sequence;
CREATE TABLE permiso (
	id bigint NOT NULL DEFAULT nextval('permiso_id_sequence'::regclass),
	nombre  text NOT NULL,
	descripcion  text NOT NULL,
	CONSTRAINT permiso_pkey PRIMARY KEY (id)
);

CREATE TABLE usuario_rol (
	usuario_id bigint NOT NULL,
	rol_id bigint NOT NULL,
	CONSTRAINT usuario_rol_pkey      PRIMARY KEY (usuario_id, rol_id),
	CONSTRAINT usuario_rol_rol_fkey     FOREIGN KEY (rol_id) REFERENCES rol(id),
	CONSTRAINT usuario_rol_usuario_fkey FOREIGN KEY (usuario_id) REFERENCES usuario(id)
);

CREATE TABLE rol_permiso (
	rol_id bigint NOT NULL,
	permiso_id bigint NOT NULL,
	CONSTRAINT rol_permiso_pkey PRIMARY KEY(rol_id, permiso_id),
	CONSTRAINT rol_permiso_rol_fkey     FOREIGN KEY (rol_id) REFERENCES rol(id),
	CONSTRAINT rol_permiso_permiso_fkey FOREIGN KEY (permiso_id) REFERENCES permiso(id)
);