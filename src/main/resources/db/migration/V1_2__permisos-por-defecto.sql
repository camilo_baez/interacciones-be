--OPCIONES
INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'CONFIGURACION_VIEW', 'Puede ver las opciones de configuración del sistema');
INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'OPERATIVO_VIEW', 'Puede ver las opciones de operación del sistema');
INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'RESULTADOS_VIEW', 'Puede ver los resultados del sistema');
INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'MOSTRAR_MAS_REGISTROS_EN_TABLA', 'Puede mostrar mas registros filtrados');

--PERMISOS
INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'USUARIO_VIEW', 'Puede ver usuarios');
INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'USUARIO_ADD', 'Puede agregar usuarios');
INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'USUARIO_EDIT', 'Puede editar usuarios');
INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'USUARIO_DELETE', 'Puede eliminar usuarios');
INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'CHANGE_ANY_USER_PASSWORD', 'Puede cambiar la contraseña de otro usuario sin saber la anterior');

INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'ROL_VIEW', 'Puede ver roles');
INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'ROL_ADD', 'Puede agregar roles');
INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'ROL_EDIT', 'Puede editar roles');
INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'ROL_DELETE', 'Puede eliminar roles');

INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'PERMISO_VIEW', 'Puede ver permisos');
INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'PERMISO_ADD', 'Puede agregar permisos');
INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'PERMISO_EDIT', 'Puede editar permisos');
INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'PERMISO_DELETE', 'Puede eliminar permisos');

INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'PACIENTE_VIEW', 'Puede ver pacientes');
INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'PACIENTE_ADD', 'Puede agregar pacientes');
INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'PACIENTE_EDIT', 'Puede editar pacientes');
INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'PACIENTE_DELETE', 'Puede eliminar pacientes');

INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'ACTIVO_VIEW', 'Puede ver activos');
INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'ACTIVO_ADD', 'Puede agregar activos');
INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'ACTIVO_EDIT', 'Puede editar activos');
INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'ACTIVO_DELETE', 'Puede eliminar activos');

INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'MEDICAMENTO_VIEW', 'Puede ver medicamentos');
INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'MEDICAMENTO_ADD', 'Puede agregar medicamentos');
INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'MEDICAMENTO_EDIT', 'Puede editar medicamentos');
INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'MEDICAMENTO_DELETE', 'Puede eliminar medicamentos');

INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'INTERACCION_VIEW', 'Puede ver interacciones');
INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'INTERACCION_ADD', 'Puede agregar interacciones');
INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'INTERACCION_EDIT', 'Puede editar interacciones');
INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'INTERACCION_DELETE', 'Puede eliminar interacciones');

INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'SEVERIDAD_VIEW', 'Puede ver severidades');
INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'SEVERIDAD_ADD', 'Puede agregar severidades');
INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'SEVERIDAD_EDIT', 'Puede editar severidades');
INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'SEVERIDAD_DELETE', 'Puede eliminar severidades');

INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'SALIDA_VIEW', 'Puede ver salidas');
INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'SALIDA_ADD', 'Puede agregar salidas');
INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'SALIDA_EDIT', 'Puede editar salidas');
INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'SALIDA_DELETE', 'Puede eliminar salidas');
--MEDICAMENTO,ACTIVO,INTERACCIONES

--RESULTADOS
INSERT INTO permiso (id,nombre,descripcion) VALUES (nextval('permiso_id_sequence'), 'INTERACCIONES_PACIENTES_VIEW', 'Puede ver el reporte de interacciones por pacientes');

--ROL
INSERT INTO rol (id,nombre,descripcion) VALUES (nextval('rol_id_sequence'),'ADMINISTRADOR','Rol de Administrador');

--USUARIO: pass = username
INSERT INTO usuario (id,nombre,username,contrasenha,tipo_documento,documento,direccion,correo,telefono)
	VALUES (nextval('usuario_id_sequence'), 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'CI', '0', '', '', '');

--ROL_PERMISO: ADMINISTRADOR
INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'CONFIGURACION_VIEW'));
INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'OPERATIVO_VIEW'));
INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'RESULTADOS_VIEW'));
INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'MOSTRAR_MAS_REGISTROS_EN_TABLA'));

INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'USUARIO_VIEW'));
INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'USUARIO_ADD'));
INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'USUARIO_EDIT'));
INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'USUARIO_DELETE'));
INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'CHANGE_ANY_USER_PASSWORD'));

INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'ROL_VIEW'));
INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'ROL_ADD'));
INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'ROL_EDIT'));
INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'ROL_DELETE'));

INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'PERMISO_VIEW'));
INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'PERMISO_ADD'));
INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'PERMISO_EDIT'));
INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'PERMISO_DELETE'));

INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'PACIENTE_VIEW'));
INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'PACIENTE_ADD'));
INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'PACIENTE_EDIT'));
INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'PACIENTE_DELETE'));

INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'ACTIVO_VIEW'));
INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'ACTIVO_ADD'));
INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'ACTIVO_EDIT'));
INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'ACTIVO_DELETE'));

INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'MEDICAMENTO_VIEW'));
INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'MEDICAMENTO_ADD'));
INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'MEDICAMENTO_EDIT'));
INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'MEDICAMENTO_DELETE'));

INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'INTERACCION_VIEW'));
INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'INTERACCION_ADD'));
INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'INTERACCION_EDIT'));
INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'INTERACCION_DELETE'));

INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'SEVERIDAD_VIEW'));
INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'SEVERIDAD_ADD'));
INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'SEVERIDAD_EDIT'));
INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'SEVERIDAD_DELETE'));

INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'SALIDA_VIEW'));
INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'SALIDA_ADD'));
INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'SALIDA_EDIT'));
INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'SALIDA_DELETE'));

INSERT INTO rol_permiso (rol_id, permiso_id) VALUES ((SELECT id FROM rol where nombre = 'ADMINISTRADOR'), (SELECT id FROM permiso where nombre = 'INTERACCIONES_PACIENTES_VIEW'));

--ROL_USUARIO
INSERT INTO usuario_rol (usuario_id, rol_id) VALUES ((SELECT id FROM usuario where nombre = 'admin'), (SELECT id FROM rol where nombre = 'ADMINISTRADOR'));
