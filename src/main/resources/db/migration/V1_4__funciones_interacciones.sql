-- Function: public.interaccionan(bigint, bigint, boolean, bigint)

-- DROP FUNCTION public.interaccionan(bigint, bigint, boolean, bigint);

CREATE OR REPLACE FUNCTION public.interaccionan(
    IN medicamento_1 bigint,
    IN medicamento_2 bigint,
    IN p_riesgo boolean,
    IN p_activo bigint)
  RETURNS TABLE(interaccion_retorno bigint) AS
$BODY$
DECLARE
id_interaccion bigint;
v_interaccion bigint;
indice integer := 1;
riesgo_1 boolean;
riesgo_2 boolean;
ACTIVOS CURSOR (p_medicamento bigint) FOR
	SELECT activo
	FROM medicamento_activo med
	join activo ac on ac.id = med.activo
	where med.medicamento = p_medicamento
	and (p_riesgo is null or coalesce(ac.alto_riesgo, false) = p_riesgo);
ACTIVOS2 CURSOR (p_medicamento bigint, desde integer) FOR
	SELECT activo
	FROM medicamento_activo med
	join activo ac on ac.id = med.activo
	where med.medicamento = p_medicamento
	and (p_riesgo is null or coalesce(ac.alto_riesgo, false) = p_riesgo) offset desde;

BEGIN
	--Busca interacciones entre los activos de un medicamento
	/*FOR r IN activos(medicamento_1) LOOP
		FOR r2 IN activos2(medicamento_1, indice) LOOP
			v_interaccion := interaccionan_activos(r.activo, r2.activo);
			IF v_interaccion is not null THEN
				interaccion_retorno := v_interaccion;
				RETURN NEXT;
			END IF;
		END LOOP;
		indice := indice +1;
	END LOOP;

	--Busca interacciones entre los activos de un medicamento
	indice := 1;
	FOR r IN activos(medicamento_2) LOOP
		FOR r2 IN activos2(medicamento_2, indice) LOOP
			v_interaccion := interaccionan_activos(r.activo, r2.activo);
			IF v_interaccion is not null THEN
				interaccion_retorno := v_interaccion;
				RETURN NEXT;
			END IF;
		END LOOP;
		indice := indice +1;
	END LOOP;
	*/
	/**
	Busca interacciones entre los activos de ambos medicamento
	*/
	FOR r IN activos(medicamento_1) LOOP
		FOR r2 IN activos2(medicamento_2, 0) LOOP
			IF p_activo is null or p_activo = r.activo or p_activo = r2.activo THEN
				v_interaccion := interaccionan_activos(r.activo, r2.activo);
				IF v_interaccion is not null THEN
					interaccion_retorno := v_interaccion;
					RETURN NEXT;
				END IF;
			END IF;
		END LOOP;
	END LOOP;
END
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION public.interaccionan(bigint, bigint, boolean, bigint)
  OWNER TO postgres;



-- Function: public.interaccionan_activos(bigint, bigint)

-- DROP FUNCTION public.interaccionan_activos(bigint, bigint);

CREATE OR REPLACE FUNCTION public.interaccionan_activos(
    activo_1 bigint,
    activo_2 bigint)
  RETURNS bigint AS
$BODY$
DECLARE
id_interaccion integer;
BEGIN
   select id into id_interaccion from interaccion
	where (activo_a = activo_1 and activo_b = activo_2)
	or (activo_a = activo_2 and activo_b = activo_1);

	RETURN id_interaccion;
END
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION public.interaccionan_activos(bigint, bigint)
  OWNER TO postgres;


-- Function: public.obtener_interacciones(text, text, bigint, bigint, bigint, text, boolean, boolean)

-- DROP FUNCTION public.obtener_interacciones(text, text, bigint, bigint, bigint, text, boolean, boolean);

CREATE OR REPLACE FUNCTION public.obtener_interacciones(
    IN p_desde text,
    IN p_hasta text,
    IN p_paciente bigint,
    IN p_medicamento bigint,
    IN p_activo bigint,
    IN p_via text,
    IN p_controlado boolean,
    IN p_riesgo boolean)
  RETURNS TABLE(paciente_ret bigint, interaccion_ret bigint, fecha_ret timestamp without time zone) AS
$BODY$
	DECLARE
	  cantidad_paciente_dia integer default 0;
	  cantidad_paciente integer default 0;
	  cantidad integer default 0;
	  numero_interaccion integer default 0;
	  indice integer default 1;
	  v_paciente bigint;
	  v_interaccion bigint;
	  v_fecha timestamp;
	  v_via_1 text;
	  v_controlado_1 boolean;
	  v_via_2 text;
	  v_controlado_2 boolean;
	  r record;
	  SALIDAS CURSOR FOR --Busca las salidas en el rango especificado
		SELECT *
		FROM salidas
		where (p_paciente is null or paciente = p_paciente)
		and fecha >= p_desde::timestamp without time zone
		and fecha <= p_hasta::timestamp without time zone;

	  SALIDAS2 CURSOR (desde integer, p_paciente bigint, p_fecha timestamp) FOR
		SELECT *
		FROM salidas
		where paciente = p_paciente
		and fecha = p_fecha offset desde;

	  INTERACCIONES CURSOR (medicamento_1 bigint, medicamento_2 bigint) FOR
		SELECT distinct on (interaccion_retorno) interaccion_retorno as id from interaccionan(medicamento_1, medicamento_2, p_riesgo, p_activo);


	BEGIN
	  FOR r IN SALIDAS LOOP
		--Si cambia de paciente, mostrar los resultados del anterior
		IF v_paciente is not null and v_paciente <> r.paciente THEN
			indice := 1;
			v_fecha := null;
			v_paciente:= null;
		END IF;

		IF v_fecha is not null and v_fecha <> r.fecha THEN
			indice := 1;
		END IF;
		v_fecha := r.fecha;
		v_paciente := r.paciente;

		FOR r2 IN SALIDAS2(indice, v_paciente, v_fecha) LOOP
			--VERIFICA FILTRO DE MEDICAMENTO
			IF not(((r.medicamento = p_medicamento or r2.medicamento = p_medicamento) and p_medicamento is not null)
				or p_medicamento is null) THEN
				continue;
			END IF;

			--VERIFICA FILTRO DE CONTROLADO Y VIA
			select controlado, via into v_controlado_1, v_via_1 from medicamento where id = r.medicamento;
			select controlado, via into v_controlado_2, v_via_2 from medicamento where id = r2.medicamento;

			-- AMBOS MEDICAMENTOS DEBEN SER CONTROLADOS
			IF not((p_controlado is null) or (p_controlado = coalesce(v_controlado_1, false) and p_controlado = coalesce(v_controlado_2, false))) THEN
				continue;
			END IF;

			-- AMBOS MEDICAMENTOS DEBEN SER DE LA MISMA VIA
			IF not((p_via is null) or (p_via = v_via_1 and p_via = v_via_2)) THEN
				continue;
			END IF;

			FOR inter IN INTERACCIONES(r.medicamento, r2.medicamento) LOOP
				v_interaccion := inter.id;
				IF v_interaccion is not null THEN
					paciente_ret:= v_paciente;
					interaccion_ret:= v_interaccion;
					fecha_ret:= r2.fecha;
					RETURN NEXT;
				END IF;
			END LOOP;

		END LOOP;

		indice := indice + 1;
	  END LOOP;
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION public.obtener_interacciones(text, text, bigint, bigint, bigint, text, boolean, boolean)
  OWNER TO postgres;
