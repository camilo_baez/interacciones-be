package py.com.interacciones.interacciones.model.report;

import java.util.Date;

import lombok.Data;

@Data
public class BaseReportModel {

	String nombreSanatorio;
	String direccionSanatorio;
	String telefonoSanatorio;
	String faxSanatorio;

	Date fechaInforme;
}
