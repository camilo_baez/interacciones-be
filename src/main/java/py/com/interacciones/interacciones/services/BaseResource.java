/*
 * @RestBaseService.java 1.0 Aug 19, 2014 
 */
package py.com.interacciones.interacciones.services;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.Entity;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import py.com.interacciones.framework.schemaForm.SchemaIgnore;
import py.com.interacciones.framework.schemaForm.SchemaNullable;
import py.com.interacciones.framework.schemaForm.SchemaProperties;
import py.com.interacciones.framework.schemaForm.SchemaProperty;
import py.com.interacciones.framework.schemaForm.SchemaType;
import py.com.interacciones.framework.util.DataWithCount;
import py.com.interacciones.interacciones.domain.BaseEntity;
import py.com.interacciones.interacciones.logic.BaseLogic;
import py.com.interacciones.interacciones.services.DataTablesHelper.QueryExtracted;

/**
 * 
 * TODO agregar soporte a loggers
 * 
 * @author sortiz
 * @since 1.0
 * @version 1.0 Aug 19, 2014
 * 
 */
public abstract class BaseResource<T extends BaseEntity> implements IBaseResource<T> {

	@Context
	UriInfo uriInfo;

	@Inject
	DataTablesHelper helper;

	@Override
	@ApiOperation(value = "Obtiene una lista paginada de entidades",
			notes = "Esto respeta el API de jquery datatables, para más detalles ver https://www.datatables.net/examples/server_side/simple.html")
	public DataWithCount<T> get(@QueryParam("draw") @DefaultValue("0") Long draw,
			@QueryParam("start") @DefaultValue("0") Integer start,
			@QueryParam("length") @DefaultValue("0") Integer length,
			@QueryParam("search[value]") @DefaultValue("") String globalSearch) {

		QueryExtracted qe = helper.extractData(uriInfo.getQueryParameters());

		DataWithCount<T> toRet = getBaseBean().get(start,
				length,
				qe.getOrders(),
				qe.getValues(),
				qe.getProperties(),
				StringUtils.isEmpty(globalSearch) ? null : globalSearch);
		toRet.setDraw(draw);
		return toRet;

	}

	@Override
	@ApiOperation("Obtiene todas las entidades")
	public List<T> getAll() {

		return getBaseBean().getAll();
	}

	@Override
	@ApiOperation("Obtiene el total de entidades")
	public long getCount() {
		return getBaseBean().getCount(Collections.emptyMap());
	}

	@Override
	@GET
	@Path("{id}")
	@ApiOperation("Recupera una entidad por su identificador")
	public T get(@PathParam("id") @ApiParam("Identificador del usuario") Long id) {

		Validate.notNull(id, "No se puede buscar una entidad con id nulo");
		return getBaseBean().findById(id);
	}

	@Override
	@ApiOperation("Permite crear una nueva entidad")
	public T add(T entity) {

		Validate.isTrue(entity.getId() == 0l, "No se puede crear una entidad con un ID ya asigando");
		T added = getBaseBean().add(entity);
		return added;
	}

	@Override
	@ApiOperation("Actualiza una entidad dado su identificador")
	public T update(@PathParam("id") Long id, T entity) {

		if (entity.getId() == 0l && id == null) {
			throw new IllegalArgumentException("ID no puede ser nulo");
		} else {
			entity.setId(id);
			return getBaseBean().update(entity);
		}
	}

	@Override
	@ApiOperation("Actualiza una entidad")
	public T update(T entity) {

		if (entity.getId() == 0) {
			throw new IllegalArgumentException("ID no puede ser nulo");
		} else {
			return getBaseBean().update(entity);
		}
	}

	@Override
	@ApiOperation("Elimina una entidad")
	public T delete(Long id) {

		if (id == 0) {
			throw new IllegalArgumentException("ID no puede ser nulo");
		} else {
			T toDelete = getBaseBean().findById(id);
			getBaseBean().remove(toDelete);
			return toDelete;
		}
	}

	@Override
	@ApiOperation(value = "Retorna un json-schema de la entidad",
			notes = "Este esquema se puede utilizar para definir las validaciones.")
	public Response getSchema() {

		JsonNodeFactory jnf = JsonNodeFactory.instance;

		ObjectNode jo = jnf.objectNode();

		jo.put("type", "object");

		ObjectNode properties = jo.putObject("properties");
		ArrayNode required = jo.putArray("required");

		Class<?> sup = getBaseBean().getClassOfT();
		while (sup.isAnnotationPresent(Entity.class) || sup.isAnnotationPresent(MappedSuperclass.class)) {
			addFields(properties, required, sup);
			sup = sup.getSuperclass();
		}

		return Response.ok().entity(jo).build();
	}

	private void addFields(ObjectNode properties, ArrayNode required, Class<?> clazz) {
		for (Field f : clazz.getDeclaredFields()) {

			if (f.getName().equals("id"))
				continue;
			if (Modifier.isFinal(f.getModifiers()))
				continue;
			if (f.isAnnotationPresent(SchemaIgnore.class))
				continue;

			ObjectNode node = properties.putObject(f.getName());

			if (f.isAnnotationPresent(SchemaType.class)) {
				node.put("type", f.getAnnotation(SchemaType.class).value());
			} else if (f.getType().isEnum()) {
				node.put("type", "string");
				ArrayNode enumValues = node.putArray("enum");
				for (Object value : f.getType().getEnumConstants()) {
					enumValues.add(value.toString());
				}
			} else {
				String typeName = f.getType().getSimpleName().toLowerCase();
				if (typeName.equals("long") || typeName.equals("short"))
					typeName = "integer";
				if (typeName.equals("double") || typeName.equals("float"))
					typeName = "number";
				node.put("type", typeName);
			}

			if (f.isAnnotationPresent(Min.class)) {
				node.put("minimun", f.getAnnotation(Min.class).value());
			}

			if (f.isAnnotationPresent(Max.class)) {
				node.put("maximum", f.getAnnotation(Max.class).value());
			}

			if (f.isAnnotationPresent(SchemaProperties.class)) {
				for (SchemaProperty schemaProperty : f.getAnnotation(SchemaProperties.class).value()) {
					node.put(schemaProperty.key(), schemaProperty.value());
				}
			}

			if (f.isAnnotationPresent(Size.class)) {
				Size size = f.getAnnotation(Size.class);
				node.put("minLength", size.min());
				node.put("maxLength", size.max());
			}

			if (f.isAnnotationPresent(Pattern.class))
				node.put("pattern", f.getAnnotation(Pattern.class).regexp());

			if (!f.isAnnotationPresent(SchemaNullable.class) && f.isAnnotationPresent(NotNull.class)) {
				required.add(f.getName());
			}

		}
	}

	protected abstract BaseLogic<T> getBaseBean();

}
