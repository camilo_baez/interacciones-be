package py.com.interacciones.interacciones.services;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import io.swagger.annotations.Api;
import py.com.interacciones.interacciones.domain.Rol;
import py.com.interacciones.interacciones.logic.BaseLogic;
import py.com.interacciones.interacciones.logic.RolLogic;

@Path("/rol")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api("/rol")
public class RolService extends BaseResource<Rol> {

	@Inject
	RolLogic logic;

	@Override
	protected BaseLogic<Rol> getBaseBean() {
		return logic;
	}

}
