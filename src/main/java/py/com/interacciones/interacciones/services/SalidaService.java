package py.com.interacciones.interacciones.services;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import py.com.interacciones.interacciones.domain.Salida;
import py.com.interacciones.interacciones.logic.BaseLogic;
import py.com.interacciones.interacciones.logic.SalidaLogic;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/salida")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api("/salida")
public class SalidaService extends BaseResource<Salida> {

	@Inject
	SalidaLogic logic;

	@Override
	protected BaseLogic<Salida> getBaseBean() {
		return logic;
	}

	@POST
	@Path("/excel")
	@Consumes("multipart/form-data")
	@ApiOperation("Entidades a partir de un archivo excel")
	public Response subirExcel(MultipartFormDataInput input) throws Exception {
		return logic.subirExcel(input);
	}

	@GET
	@Path("/excel")
	@ApiOperation("Prueba")
	public Response ok() throws Exception {
		return Response.ok().build();
	}

	@DELETE
	@Path("/rango/{desde}/{hasta}")
	@ApiOperation("Eliminar salidas a partir de un rango de fechas")
	public int eliminarRango(@PathParam("desde") String desde,
								  @PathParam("hasta") String hasta) throws Exception {
		return logic.eliminarRango(desde, hasta);
	}
}
