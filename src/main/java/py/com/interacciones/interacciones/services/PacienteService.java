package py.com.interacciones.interacciones.services;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import py.com.interacciones.interacciones.domain.Paciente;
import py.com.interacciones.interacciones.logic.BaseLogic;
import py.com.interacciones.interacciones.logic.PacienteLogic;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/paciente")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api("/paciente")
public class PacienteService extends BaseResource<Paciente> {

	@Inject
	PacienteLogic logic;

	@Override
	protected BaseLogic<Paciente> getBaseBean() {
		return logic;
	}

	@GET
	@Path("/prueba")
	@ApiOperation("Prueba")
	public Response ok() throws Exception {
		return Response.ok().build();
	}
}
