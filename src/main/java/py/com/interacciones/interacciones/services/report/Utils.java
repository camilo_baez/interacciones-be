package py.com.interacciones.interacciones.services.report;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.text.StrSubstitutor;

import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.core.layout.HorizontalBandAlignment;
import ar.com.fdvs.dj.domain.AutoText;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.Style;
import ar.com.fdvs.dj.domain.builders.DynamicReportBuilder;
import ar.com.fdvs.dj.domain.builders.StyleBuilder;
import ar.com.fdvs.dj.domain.constants.Font;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;
import py.com.interacciones.interacciones.config.Constants;

/**
 * Clase que almacena funcionalidades comunes para los reportes.
 *
 * @author sortiz
 *
 */
public class Utils {

	public static final String PERIODO_DATE_FORMAT = "yyyyMM";

	public static enum Formato {
		PDF("application/pdf"), JSON(MediaType.APPLICATION_JSON), XLSX(
				"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"), TXT("text/plain:");

		private String mimeType;

		Formato(String mimeType) {
			this.mimeType = mimeType;
		}

		public String getMime() {
			return mimeType;
		}

		/**
		 * Permite que se admitan enums en minúscula.
		 *
		 * @param param
		 * @return
		 */
		public static Formato fromString(String param) {
			String toUpper = param.toUpperCase();
			try {
				return valueOf(toUpper);
			} catch (Exception e) {
				throw new WebApplicationException(400);
			}
		}
	}

	/**
	 * Exporta el reporte para que luego sea metido en un response.
	 *
	 * Sin datasource.
	 *
	 * @param dr
	 *            reporte en sí.
	 * @param parametros
	 *            parámetros extras
	 * @param tipo
	 *            tipo de reporte
	 * @param nombre
	 *            nombre del reporte (se usa tambien para el nombre de la hoja
	 *            si es excel).
	 * @return byte array del reporte, nunca null
	 * @throws JRException
	 *             si ocurrio algun error
	 */
	public static byte[] generarArchivoReporte(DynamicReport dr, Map<String, Object> parametros, Formato tipo,
			String nombre) throws JRException {

		return generarArchivoReporte(dr, parametros, tipo, nombre, null);
	}

	/**
	 * Exporta el reporte para que luego sea metido en un response.
	 *
	 * @param dr
	 *            reporte en sí.
	 * @param parametros
	 *            parámetros extras
	 * @param tipo
	 *            tipo de reporte
	 * @param nombre
	 *            nombre del reporte (se usa tambien para el nombre de la hoja
	 *            si es excel).
	 * @return byte array del reporte, nunca null
	 * @throws JRException
	 *             si ocurrio algun error
	 */
	public static <T> byte[] generarArchivoReporte(DynamicReport dr, Map<String, Object> parametros, Formato tipo,
			String nombre, List<T> datasource) throws JRException {

		JasperPrint jp;

		if (tipo == Formato.XLSX)
			parametros.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);

		if (datasource == null) {
			JasperReport jr = DynamicJasperHelper.generateJasperReport(dr, new ClassicLayoutManager(), parametros);
			jp = JasperFillManager.fillReport(jr, parametros);
		} else {
			jp = DynamicJasperHelper.generateJasperPrint(dr,
					new ClassicLayoutManager(),
					new JRBeanCollectionDataSource(datasource),
					parametros);
		}

		try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
			switch (tipo) {
			case PDF:

				JRPdfExporter exporter = new JRPdfExporter();
				exporter.setExporterInput(new SimpleExporterInput(jp));
				exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(baos));
				exporter.exportReport();
				break;
			case XLSX:

				SimpleXlsxReportConfiguration excelConfig = new SimpleXlsxReportConfiguration();
				excelConfig.setIgnoreGraphics(false);
				excelConfig.setOnePagePerSheet(false);
				excelConfig.setRemoveEmptySpaceBetweenRows(true);
				excelConfig.setDetectCellType(true);
				excelConfig.setForcePageBreaks(false);
				excelConfig.setFitWidth(100);
				excelConfig.setSheetNames(new String[] { nombre });

				JRXlsxExporter xExporter = new JRXlsxExporter();
				xExporter.setConfiguration(excelConfig);
				xExporter.setExporterInput(new SimpleExporterInput(jp));
				xExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(baos));
				xExporter.exportReport();
				break;

			default:
				throw new IllegalAccessError(tipo.toString());
			}
			return baos.toByteArray();
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	/**
	 * Crea el autotex para la paginación
	 *
	 * @param drb
	 */
	public static void addNumeroPagina(DynamicReportBuilder drb, Style footerStyle) {
		AutoText text = new AutoText(AutoText.AUTOTEXT_PAGE_X_OF_Y,
				AutoText.POSITION_FOOTER,
				HorizontalBandAlignment.buildAligment(AutoText.ALIGMENT_LEFT));
		text.setWidth(300);
		text.setWidth2(300);
		text.setHeight(10);
		text.setStyle(footerStyle);
		drb.addAutoText(text);
	}

	/**
	 * Crea el footer con el generado el y la paginación
	 *
	 * @param drb
	 */
	public static void addFooter(DynamicReportBuilder drb, Formato type) {
		Style footerStyle = new StyleBuilder(true).setFont(new Font(8, Constants.REPORT_FONT, false, false, false))
				.setTextColor(Color.black)
				.build();
		if (type.equals(Formato.PDF))
			addNumeroPagina(drb, footerStyle);

		addGeneradoEl(drb, footerStyle);
	}

	/**
	 * Agrega una etiqueta al final a la derecha con al fecha de creación del
	 * reporte.
	 *
	 * @param frb
	 */
	public static void addGeneradoEl(DynamicReportBuilder frb, Style footerStyle) {

		Calendar cReal = Calendar.getInstance();
		Calendar c = Calendar.getInstance(TimeZone.getTimeZone("America/Asuncion"));

		c.set(cReal.get(Calendar.YEAR),
				cReal.get(Calendar.MONTH),
				cReal.get(Calendar.DATE),
				cReal.get(Calendar.HOUR),
				cReal.get(Calendar.MINUTE),
				cReal.get(Calendar.SECOND));

		String fecha = new SimpleDateFormat("dd' de 'MMMM' del 'YYYY' a las 'HH:mm", new Locale("ES", "PY"))
				.format(c.getTime());
		AutoText text = new AutoText("Creado el " + fecha,
				AutoText.POSITION_FOOTER,
				HorizontalBandAlignment.buildAligment(AutoText.ALIGMENT_RIGHT));
		text.setWidth(300);
		text.setWidth2(300);
		text.setHeight(10);
		text.setStyle(footerStyle);
		frb.addAutoText(text);
	}

	/**
	 * Agrega un header con el subtitulo del reporte y los parametros usados
	 * reporte.
	 *
	 * @param frb
	 * @param params
	 */
	public static void addHeader(DynamicReportBuilder frb, Map<String, String> params) {
		addHeader(frb, Constants.TEMPLATE_HEADER_INFORME_CONSULTA, params);
	}

	public static void addHeader(DynamicReportBuilder frb, String template, Map<String, String> params) {

		Style headerStyle = new StyleBuilder(true).setFont(new Font(8, Constants.REPORT_FONT, false, false, false))
				.setTextColor(Color.black)
				.build();

		AutoText text = new AutoText(createStringFromMap(params, template),
				AutoText.POSITION_HEADER,
				HorizontalBandAlignment.buildAligment(AutoText.ALIGMENT_RIGHT));

		text.setStyle(headerStyle);
		text.setWidth(300);
		text.setWidth2(300);
		text.setHeight(10);
		frb.addAutoText(text);
	}

	/**
	 * Crea un string según el map para poner en el header
	 *
	 * @param params
	 * @param template
	 */
	public static String createStringFromMap(Map<String, String> params, String template) {
		StrSubstitutor sub = new StrSubstitutor(params, "%(", ")");
		String result = sub.replace(template);
		return result;
	}

	/**
	 * Retorna el periodo al que pertenece un anho y un mes.
	 *
	 * @param anho
	 * @param mes
	 * @return
	 */
	public static String getPeriodo(Integer anho, Integer mes) {
		String rigthPart = mes.toString();
		if (mes < 10)
			rigthPart = "0" + rigthPart;
		return new StringBuilder().append(anho).append(rigthPart).toString();
	}

	public static Date getFrom(Integer anho, Integer mes) {

		return getFrom(anho, mes, -1);
	}

	/**
	 * Retorna el primer dia de un periodo.
	 * 
	 * @param periodo
	 *            periodo no nulo ni vacio
	 * @return
	 */
	public static Date getFirstDayOfPeriod(String periodo) {
		Validate.notBlank(periodo, "Periodo invalido");
		try {
			return new SimpleDateFormat(PERIODO_DATE_FORMAT).parse(periodo);
		} catch (Exception e) {
			throw new IllegalArgumentException("Fecha inválida " + periodo, e);
		}
	}

	public static Date getFrom(Integer anho, Integer mes, Integer dia) {

		if (dia > 0) {
			Calendar c = Calendar.getInstance();
			c.setLenient(false);
			c.set(anho, mes - 1, dia);
			c.getTime();
			c.set(Calendar.HOUR, 0);
			c.set(Calendar.MINUTE, 0);
			c.set(Calendar.SECOND, 0);
			return c.getTime();
		} else {
			try {
				return new SimpleDateFormat(PERIODO_DATE_FORMAT).parse(getPeriodo(anho, mes));
			} catch (Exception e) {
				throw new IllegalArgumentException("Fecha inválida " + getPeriodo(anho, mes), e);
			}
		}
	}

	/**
	 * Retorna el último segundo del dia del mes pedido .
	 *
	 */
	public static Date getTo(Integer anho, Integer mes) {

		return getTo(anho, mes, -1);
	}

	/**
	 * Retorna el último segundo del dia pedido (si no hay dia, el ultimo dia
	 * del mes, en su ultimo segundo).
	 *
	 */
	public static Date getTo(Integer anho, Integer mes, Integer dia) {

		if (dia == null || dia < 0) {
			try {
				Date d = new SimpleDateFormat(PERIODO_DATE_FORMAT).parse(Validate.notBlank(getPeriodo(anho, mes)));
				Calendar c = Calendar.getInstance();
				c.setTime(d);
				c.add(Calendar.MONTH, 1);
				c.getTime();
				c.add(Calendar.SECOND, -1);
				return c.getTime();
			} catch (Exception e) {
				throw new IllegalArgumentException("Fecha inválida " + getPeriodo(anho, mes), e);
			}
		} else {
			Calendar c = Calendar.getInstance();
			c.setLenient(true);
			c.clear();
			c.getTime();
			c.set(anho, mes - 1, dia + 1);
			c.getTime();
			c.add(Calendar.SECOND, -1);
			return c.getTime();
		}
	}

}
