package py.com.interacciones.interacciones.services;

import io.swagger.annotations.Api;
import py.com.interacciones.interacciones.domain.Severidad;
import py.com.interacciones.interacciones.logic.BaseLogic;
import py.com.interacciones.interacciones.logic.SeveridadLogic;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/severidad")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api("/severidad")
public class SeveridadService extends BaseResource<Severidad> {

	@Inject
	SeveridadLogic logic;

	@Override
	protected BaseLogic<Severidad> getBaseBean() {
		return logic;
	}

}
