package py.com.interacciones.interacciones.services;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import py.com.interacciones.interacciones.logic.PacienteLogic;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/resultados")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api("/resultados")
public class ResultadosService {

	@Inject
	PacienteLogic pacienteLogic;

	@GET
	@Path("/interacciones")
	@ApiOperation("Obneter las interacciones encontradas en un paciente")
	public Response subirExcel(@QueryParam("desde") String desde,
							   @QueryParam("hasta") String hasta,
							   @QueryParam("idPaciente") Long idPaciente,
							   @QueryParam("idMedicamento") Long idMedicamento,
							   @QueryParam("idActivo") Long idActivo,
							   @QueryParam("via") String via,
							   @QueryParam("controlado") Boolean controlado,
							   @QueryParam("riesgo") Boolean riesgo) throws Exception {

		return pacienteLogic.getInteracciones(idPaciente, idMedicamento, idActivo,
				via, riesgo, controlado, desde, hasta);
	}
}
