package py.com.interacciones.interacciones.services;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.Validate;

import com.fasterxml.jackson.databind.JsonNode;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import py.com.interacciones.interacciones.domain.Usuario;
import py.com.interacciones.interacciones.logic.BaseLogic;
import py.com.interacciones.interacciones.logic.UsuarioLogic;
import py.com.interacciones.framework.security.Require;

@Path("/usuario")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api("/usuario")
public class UsuarioService extends BaseResource<Usuario> {

	@Inject
	UsuarioLogic logic;

	@Override
	protected BaseLogic<Usuario> getBaseBean() {
		return logic;
	}

	@GET
	@Path("{id}")
	@ApiOperation("Recupera una entidad por su identificador")
	@Override
	public Usuario get(@PathParam("id") @ApiParam("Identificador del usuario") Long id) {

		return logic.getWithRoles(Validate.notNull(id, "Usuario no encontrado"));
	}

	@POST
	@Path("/addNoPass")
	public Usuario addWithoutPass(Usuario user) {
		Validate.notNull(user, "Usuario inválido");
		user.setContrasenha("123456");
		user.setCambiarContrasenha(true);
		return super.add(user);
	}

	@GET
	@ApiOperation("Retorna la lista de usuarios que cumplen con el nombre")
	@Path("/filtered")
	public List<Usuario> getUsuariosWithName(@QueryParam("query") String name) {
		return logic.getNombreDeUsuarios(name);
	}

	@POST
	@Path("{id}/changePassword")
	public Usuario changePassword(@PathParam("id") Long id, ChangePassModel model) {

		Validate.notNull(id, "Usuario inválido");

		return logic.changePass(id, model.getOldPass(), model.getNewPass(), model.getRepeatNewPass());
	}

	@GET
	@Path("{id}/forceChange")
	@Require(UsuarioLogic.PERMISSION_TO_CHANGE_ANY_PASS)
	public Usuario forceChangePass(@PathParam("id") Long id) {
		Validate.notNull(id, "Usuario inválido");
		return logic.makeChangePass(id);
	}

	@PUT
	@Path("{id}/configuraciones")
	public Usuario updateConfiguraciones(@PathParam("id") @ApiParam("Identificador del usuario") Long id,
			JsonNode config) {
		Validate.notNull(id, "Usuario inválido");
		return logic.updateConfig(id, config);
	}

	@GET
	@Path("/conPermiso")
	public List<Usuario> getConPermiso(@QueryParam("permiso") String permiso) {

		Validate.notBlank(permiso, "El permiso esta vacio");
		return logic.getUsuariosWithPermission(permiso).stream().map(e -> {
			Usuario ligero = new Usuario();
			ligero.setNombre(e.getNombre());
			ligero.setUsername(e.getUsername());
			ligero.setId(e.getId());
			return ligero;
		}).collect(Collectors.toList());
	}
	
	@GET
	@Path("/withGroup")
	public List<Usuario> getWithGroup() {
		return logic.getWithGroup();
	}

	@Data
	public static class ChangePassModel {
		String newPass;
		String oldPass;
		String repeatNewPass;
	}
}
