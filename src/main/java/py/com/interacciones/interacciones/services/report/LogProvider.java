package py.com.interacciones.interacciones.services.report;

import java.io.File;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.io.FileUtils;

import io.swagger.annotations.Api;
import lombok.Value;
import py.com.interacciones.framework.util.Config;

/**
 * 
 * @author sortiz
 *
 */
@Api("debug")
@Path("debug/log")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class LogProvider {
	
	@Inject
	private Config config;

	@GET
	public Result getLog() {

		List<Data> result = processFile(readFile()).stream().sorted(Comparator.comparing(Data::getDate).reversed())
				.collect(Collectors.toList());

		Data last = result.get(0);
		return new Result(last.getDate(), last, result.stream().filter(Data::hasError).collect(Collectors.toList()));
	}

	private List<String> readFile() {

		try {
			return FileUtils.readLines(new File(config.getLogLocation()));
		} catch (IOException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}

	public List<Data> processFile(List<String> file) {

		List<String> filtered = file.stream().filter(e -> e.startsWith("[")).collect(Collectors.toList());

		Map<String, List<String>> data = filtered.stream().collect(Collectors.groupingBy(i -> i.substring(1, 17),
				Collectors.mapping(i -> i.substring(21), Collectors.toList())));

		return data.entrySet().stream().map(this::convert).collect(Collectors.toList());
	}

	public Data convert(Map.Entry<String, List<String>> data) {

		String fecha = data.getKey();
		String db = "UNKNOW";
		String nginx = "UNKNOW";
		String wildfly = "UNKNOW";
		String accesible = "UNKNOW";
		for (String s : data.getValue()) {
			String filtered = s.substring(s.lastIndexOf("]") + 1).trim();
			if (s.contains("db"))
				db = filtered;
			if (s.contains("nginx"))
				nginx = filtered;
			if (s.contains("wildfly"))
				wildfly = filtered;
			if (s.contains("Accesible"))
				accesible = filtered;
		}
		return new Data(fecha, accesible, nginx, wildfly, db);
	}

	@Value
	public static class Result {
		String lastCheck;
		Data last;
		List<Data> data;
	}

	@Value
	public static class Data {
		String date;
		String accesible;
		String nginx;
		String wildfly;
		String db;

		public boolean hasError() {
			return accesible.contains("Error") || nginx.contains("Error") || wildfly.contains("Error")
					|| db.contains("Error");

		}
	}

}
