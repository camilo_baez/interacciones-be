package py.com.interacciones.interacciones.services;

import io.swagger.annotations.Api;
import py.com.interacciones.interacciones.domain.Medicamento;
import py.com.interacciones.interacciones.logic.BaseLogic;
import py.com.interacciones.interacciones.logic.MedicamentoLogic;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/medicamento")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api("/medicamento")
public class MedicamentoService extends BaseResource<Medicamento> {

	@Inject
	MedicamentoLogic logic;

	@Override
	protected BaseLogic<Medicamento> getBaseBean() {
		return logic;
	}

}
