/*
 * @IRestBaseService.java 1.0 Sep 3, 2014
 * Sistema de Gestion de Procesos
 *
 */
package py.com.interacciones.interacciones.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import py.com.interacciones.framework.util.DataWithCount;
import py.com.interacciones.interacciones.domain.BaseEntity;

/**
 *
 * @author sortiz
 * @since 1.0
 * @version 1.0 Sep 3, 2014
 *
 */
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface IBaseResource<T extends BaseEntity> {

	@GET
	DataWithCount<T> get(@QueryParam("draw") @DefaultValue("0") Long draw,
			@QueryParam("start") @DefaultValue("0") Integer start,
			@QueryParam("length") @DefaultValue("0") Integer length,
			@QueryParam("search[value]") @DefaultValue("") String globalSearch);

	@GET
	@Path("/all")
	List<T> getAll();

	@GET
	@Path("/count")
	long getCount();

	@GET
	@Path("{id}")
	T get( @PathParam("id") Long id);

	@POST
	T add(T entity);

	@PUT
	@Path("/{id}")
	T update(@PathParam("id") Long id,  T entity);

	@PUT
	T update( T entity);

	@DELETE
	@Path("/{id}")
	T delete(@PathParam("id") Long id);

	@GET
	@Path("schema/")
	Response getSchema();

}
