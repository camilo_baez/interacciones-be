package py.com.interacciones.interacciones.services;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import py.com.interacciones.interacciones.domain.Interaccion;
import py.com.interacciones.interacciones.logic.BaseLogic;
import py.com.interacciones.interacciones.logic.InteraccionLogic;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/interaccion")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api("/interaccion")
public class InteraccionService extends BaseResource<Interaccion> {

	@Inject
	InteraccionLogic logic;

	@Override
	protected BaseLogic<Interaccion> getBaseBean() {
		return logic;
	}

	@POST
	@Path("/excel")
	@Consumes("multipart/form-data")
	@ApiOperation("Entidades a partir de un archivo excel")
	public Response subirExcel(MultipartFormDataInput input) throws Exception {
		return logic.subirExcel(input);
	}
}
