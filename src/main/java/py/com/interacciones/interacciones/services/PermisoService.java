package py.com.interacciones.interacciones.services;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import io.swagger.annotations.Api;
import py.com.interacciones.interacciones.domain.Permiso;
import py.com.interacciones.interacciones.logic.BaseLogic;
import py.com.interacciones.interacciones.logic.PermisoLogic;

@Path("/permiso")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api("/permiso")
public class PermisoService extends BaseResource<Permiso> {

	@Inject
	PermisoLogic logic;

	@Override
	protected BaseLogic<Permiso> getBaseBean() {
		return logic;
	}

}
