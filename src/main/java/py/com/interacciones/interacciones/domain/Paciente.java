package py.com.interacciones.interacciones.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
@Table(name = "paciente", schema = "public")
@EqualsAndHashCode(callSuper = true)
@SequenceGenerator(name = "PacienteIdGenerator", sequenceName = "paciente_id_seq", allocationSize = 1)
public class Paciente extends BaseEntity {

	private static final long serialVersionUID = 2440270145037138198L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PacienteIdGenerator")
	private long id;

	@NotNull
	private Long codigo;

	@NotNull
	private String nombre;

}