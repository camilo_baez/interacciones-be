package py.com.interacciones.interacciones.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Table(name = "medicamento", schema = "public")
@EqualsAndHashCode(callSuper = true)
@SequenceGenerator(name = "MedicamentoIdGenerator", sequenceName = "medicamento_id_seq", allocationSize = 1)
public class Medicamento extends BaseEntity {
    public static enum VIA {
        IM, //Intramuscular (IM)
        IV, //Intravenosa (IV)
        SC, //Subcutánea (SC)
        VI, //Inhalatoria (VI)
        VO, //Oral (VO)
        VR, //Rectal (VR)
        VT, //Tópica (VT)
        VV  //Vaginal (VV)
    }

    private static final long serialVersionUID = 2440270145037138198L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MedicamentoIdGenerator")
    private long id;
    @NotNull
    private Long codigo;
    @NotNull
    private String nombre;
    private String descripcion;
    private String tipo;
    @Enumerated(EnumType.STRING)
    private VIA via;
    private Integer cantidad;
    @Column(name = "unidad_medida")
    private String unidadMedida;
    private String concentracion;
    @Column(name = "unidad_medida_concentracion")
    private String unidadMedidaConcentracion;

    private Boolean controlado;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "medicamento_activo", joinColumns = {
            @JoinColumn(name = "medicamento", referencedColumnName = "id")}, inverseJoinColumns = {
            @JoinColumn(name = "activo", referencedColumnName = "id")})
    private List<Activo> activos;

    public List<Activo> getActivos() {
        if (this.activos == null)
            this.activos = new ArrayList<>();
        return this.activos;
    }

}