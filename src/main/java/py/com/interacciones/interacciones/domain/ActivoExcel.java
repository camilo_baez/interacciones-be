package py.com.interacciones.interacciones.domain;

import lombok.Data;

@Data
public class ActivoExcel {

	private String activo;

	private String descripcion;

}