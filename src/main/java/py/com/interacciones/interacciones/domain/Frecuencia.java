package py.com.interacciones.interacciones.domain;

import lombok.Data;

import java.math.BigInteger;

@Data
public class Frecuencia {
    private Integer cantidad;
    private BigInteger codigo;
    private String nombre;
}
