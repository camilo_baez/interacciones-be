package py.com.interacciones.interacciones.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Data
@Table(name = "salidas", schema = "public")
@EqualsAndHashCode(callSuper = true)
@SequenceGenerator(name = "SalidaIdGenerator", sequenceName = "salidas_id_seq", allocationSize = 1)
public class Salida extends BaseEntity {
	private static final long serialVersionUID = 2440270145037138198L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SalidaIdGenerator")
	private long id;

	@NotNull
	private Long codigo;

	@NotNull
	private Integer cantidad;

	@NotNull
	@Column(name = "fecha")
	private Date fecha;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "paciente")
	//@JsonIgnoreProperties({"pais", "departamento", "distrito", "localidad"})
	private Paciente paciente;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "medicamento")
	//@JsonIgnoreProperties({"pais", "departamento", "distrito", "localidad"})
	private Medicamento medicamento;
}