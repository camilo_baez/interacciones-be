package py.com.interacciones.interacciones.domain.converters;

import java.io.IOException;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module;

@Converter(autoApply = true)
public class JsonConverter implements AttributeConverter<JsonNode, String> {

	static ObjectMapper om;

	static {
		om = new ObjectMapper();
		om.registerModule(new Hibernate4Module());
	}

	@Override
	public String convertToDatabaseColumn(JsonNode attribute) {

		try {
			return om.writeValueAsString(attribute);
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public JsonNode convertToEntityAttribute(String dbData) {
		try {
			if (dbData == null) {
				// solo funciona is se utiliza como objetos.
				return JsonNodeFactory.instance.objectNode();
			}
			return om.readTree(dbData);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
