package py.com.interacciones.interacciones.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
@Table(name = "interaccion", schema = "public")
@EqualsAndHashCode(callSuper = true, of = "id")
@SequenceGenerator(name = "InteraccionIdGenerator", sequenceName = "interaccion_id_seq", schema = "public", allocationSize = 1)
public class Interaccion extends BaseEntity {

	private static final long serialVersionUID = -6823571831844219152L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "InteraccionIdGenerator")
	private long id;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "activo_a")
//	@JsonManagedReference
	private Activo activoA;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "activo_b")
//	@JsonManagedReference
	private Activo activoB;

	@ManyToOne
	@JoinColumn(name = "severidad")
//	@JsonManagedReference
	private Severidad severidad;

	private String efecto;
}