package py.com.interacciones.interacciones.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
@Table(name = "severidad", schema = "public")
@EqualsAndHashCode(callSuper = true)
@SequenceGenerator(name = "SeveridadIdGenerator", sequenceName = "severidad_id_seq", allocationSize = 1)
public class Severidad extends BaseEntity {

	private static final long serialVersionUID = 2440270145037138198L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SeveridadIdGenerator")
	private long id;

	private Integer nivel;
	@NotNull
	private String nombre;

	private String descripcion;

}