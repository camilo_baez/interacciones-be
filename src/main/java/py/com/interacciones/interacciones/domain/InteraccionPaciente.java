package py.com.interacciones.interacciones.domain;

import lombok.Data;

import java.math.BigInteger;
import java.util.Date;

@Data
public class InteraccionPaciente {

	private BigInteger idPaciente;

	private BigInteger codigoPaciente;

	private String paciente;

	private String activos;

	private BigInteger idActivoA;

	private BigInteger idActivoB;

	private String activoA;

	private String activoB;

	private BigInteger idInteraccion;

	private Date fecha;

	private String severidad;

	private BigInteger idSeveridad;

}