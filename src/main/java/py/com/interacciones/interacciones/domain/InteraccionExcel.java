package py.com.interacciones.interacciones.domain;

import lombok.Data;

@Data
public class InteraccionExcel {
	private Long severidadCodigo;

	private String activoA;

	private String activoB;

	private String efecto;

	private String severidad;

}