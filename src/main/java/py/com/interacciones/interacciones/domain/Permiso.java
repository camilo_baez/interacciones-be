package py.com.interacciones.interacciones.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@Table(name = "permiso", schema = "public")
@EqualsAndHashCode(callSuper = true)
@SequenceGenerator(name = "PermisoIdGenerator", sequenceName = "permiso_id_sequence", schema = "public", allocationSize = 1)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Permiso extends BaseEntity {
	
	private static final long serialVersionUID = 6306952136977994872L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "PermisoIdGenerator")
	private long id;
	
	@NotNull
	@Size(max = 100, min = 3)
	private String nombre;

	@NotNull
	@Size(max = 100, min = 6)
	private String descripcion;
	
}
