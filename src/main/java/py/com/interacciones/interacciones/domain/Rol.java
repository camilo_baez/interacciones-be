package py.com.interacciones.interacciones.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@Table(name = "rol", schema = "public")
@EqualsAndHashCode(callSuper = true)
@SequenceGenerator(name = "RolIdGenerator", sequenceName = "rol_id_sequence", schema = "public", allocationSize = 1)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Rol extends BaseEntity {

	private static final long serialVersionUID = -6823571831844219152L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "RolIdGenerator")
	private long id;

	@NotNull
	@Size(max = 100, min = 3)
	private String nombre;

	@NotNull
	@Size(max = 100, min = 6)
	private String descripcion;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "rol_permiso", joinColumns = {
			@JoinColumn(name = "rol_id", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "permiso_id", referencedColumnName = "id") })
	private List<Permiso> permisos;

	public List<Permiso> getPermisos() {

		if (this.permisos == null)
			this.permisos = new ArrayList<>();
		return this.permisos;
	}
}

