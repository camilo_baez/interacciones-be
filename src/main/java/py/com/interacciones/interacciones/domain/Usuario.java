package py.com.interacciones.interacciones.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@Table(name = "usuario", schema = "public")
@EqualsAndHashCode(callSuper = true, of = "id")
@SequenceGenerator(name = "UsuarioIdGenerator", sequenceName = "usuario_id_sequence", schema = "public", allocationSize = 1)
public class Usuario extends BaseEntity {

	private static final long serialVersionUID = -6823571831844219152L;
	
	public static enum TipoDocumento {
		CI, RUC, PASAPORTE
	}
	
	public static enum Estado {
		ACTIVO, INACTIVO
	}

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "UsuarioIdGenerator")
	private long id;
	
	@NotNull
	@Size(max = 100, min = 3)
	private String username;

	@NotNull
	@Size(max = 100, min = 3)
	private String nombre;
	
	@Size(max = 200, min = 0)
	private String direccion;
	
	@Size(max = 100, min = 0)
	private String telefono;

	@Size(max = 100, min = 0)
	private String correo;

	@NotNull
	@Size(max = 100, min = 6)
	@JsonIgnore
	private String contrasenha;
	
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name="tipo_documento")
	private TipoDocumento tipoDocumento;
	
	@NotNull
	@Size(max = 50, min = 1)
	private String documento;
	
	@Column(columnDefinition = "json")
	private JsonNode configuraciones;

	@ManyToMany
	@JoinTable(name = "usuario_rol", joinColumns = {
			@JoinColumn(name = "usuario_id", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "rol_id", referencedColumnName = "id") })
	private List<Rol> roles;

	@Enumerated(EnumType.STRING)
	private Estado estado;

	@Column(name = "cambiar_contrasenha")
	private boolean cambiarContrasenha;

	public List<Rol> getRoles() {

		if (this.roles == null)
			this.roles = new ArrayList<>();
		return this.roles;
	}

	@PrePersist
	@PreUpdate
	void checkJson() {
		if (getConfiguraciones() == null)
			setConfiguraciones(JsonNodeFactory.instance.objectNode());

		if (nombre != null)
			nombre = nombre.toUpperCase();
	}
}