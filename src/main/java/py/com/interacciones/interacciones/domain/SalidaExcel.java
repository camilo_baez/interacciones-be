package py.com.interacciones.interacciones.domain;

import lombok.Data;

import java.util.Date;

@Data
public class SalidaExcel {
	private Long codigo;

	private Integer cantidad;

	private Date fecha;

	private Long paciente;

	private String pacienteNombre;

	private Long medicamento;

	private String medicamentoNombre;

	private long fila;

}