package py.com.interacciones.interacciones.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
@Table(name = "activo", schema = "public")
@EqualsAndHashCode(callSuper = true)
@SequenceGenerator(name = "ActivoIdGenerator", sequenceName = "activo_id_seq", allocationSize = 1)
public class Activo extends BaseEntity {

	private static final long serialVersionUID = 2440270145037138198L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ActivoIdGenerator")
	private long id;

	@NotNull
	private String nombre;

	private String descripcion;

	@Column(name = "alto_riesgo")
	private Boolean altoRiesgo;

}