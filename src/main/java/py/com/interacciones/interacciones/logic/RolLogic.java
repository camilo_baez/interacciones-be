package py.com.interacciones.interacciones.logic;

import javax.ejb.Stateless;

import py.com.interacciones.interacciones.domain.Rol;

@Stateless
public class RolLogic extends BaseLogic<Rol> {

	public Rol getRolCaja() {
		return getStreamAll().where(e -> e.getNombre().equals("CAJA")).getOnlyValue();
	}

}
