package py.com.interacciones.interacciones.logic;

import org.apache.commons.lang3.Validate;
import py.com.interacciones.interacciones.domain.Medicamento;

import javax.ejb.Stateless;

@Stateless
public class MedicamentoLogic extends BaseLogic<Medicamento> {

    @Override
    public Medicamento add(Medicamento entity) {
        return super.add(entity);
    }

    //@formatter:off
    private static final String QUERY = ""
            + " SELECT e"
            + " FROM Medicamento e"
            + " WHERE e.codigo = :codigo";
    //@formatter:on
    public Medicamento getByCodigo(Long codigo) {
        Validate.notNull(codigo, "No se puede buscar por código nulo");
        try{
            return getEm().createQuery(QUERY, Medicamento.class).setParameter("codigo", codigo)
                    .getSingleResult();
        }catch (Exception ex){
            return null;
        }

    }

}