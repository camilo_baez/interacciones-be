package py.com.interacciones.interacciones.logic;

import org.apache.commons.lang3.Validate;
import py.com.interacciones.interacciones.domain.Severidad;

import javax.ejb.Stateless;

@Stateless
public class SeveridadLogic extends BaseLogic<Severidad> {

    @Override
    public Severidad update(Severidad entity) {
        if(entity.getNombre() != null)
            entity.setNombre(entity.getNombre().trim());
        return super.update(entity);
    }
    @Override
    public Severidad add(Severidad entity) {
        if(entity.getNombre() != null)
            entity.setNombre(entity.getNombre().trim());
        return super.add(entity);
    }

    //@formatter:off
    private static final String QUERY = ""
            + " SELECT e"
            + " FROM Severidad e"
            + " WHERE e.nombre = :nombre";
    //@formatter:on
    public Severidad getByNombre(String nombre) {
        try {
            Validate.notNull(nombre, "No se puede buscar por nombre vacío");
            return getEm().createQuery(QUERY, Severidad.class).setParameter("nombre", nombre)
                    .getSingleResult();
        }catch (Exception ex){
            return null;
        }
    }

}