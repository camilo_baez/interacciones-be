package py.com.interacciones.interacciones.logic;

import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import py.com.interacciones.interacciones.domain.Activo;
import py.com.interacciones.interacciones.domain.Interaccion;
import py.com.interacciones.interacciones.domain.InteraccionExcel;
import py.com.interacciones.interacciones.domain.Severidad;
import py.com.interacciones.framework.util.ExcelInteraccion;
import py.com.interacciones.framework.util.Utils;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Stateless
public class InteraccionLogic extends BaseLogic<Interaccion> {

    @Inject
    ActivoLogic activoLogic;

    @Inject
    SeveridadLogic severidadLogic;

    //@formatter:off
    private static final String QUERY = ""
            + " SELECT e"
            + " FROM Interaccion e"
            + " WHERE (e.activoA.id = :activoA and e.activoB.id = :activoB) or "
            + "       (e.activoA.id = :activoB and e.activoB.id = :activoA)";
    //@formatter:on
    public Interaccion getByActivos(Long activoA, Long activoB) {
        try {
            return getEm().createQuery(QUERY, Interaccion.class)
                    .setParameter("activoA", activoA)
                    .setParameter("activoB", activoB)
                    .getResultList().get(0);
        }catch (Exception ex){
            return null;
        }
    }
    
    public Response subirExcel(MultipartFormDataInput input) throws Exception{
        HashMap<String, InputStream> map = Utils.getData(input);
        ExcelInteraccion excel = new ExcelInteraccion();
        Map.Entry<String, InputStream> entry = map.entrySet().iterator().next();
        List<InteraccionExcel> interaccionExcels = excel.obtenerDelExcel(entry.getKey(), entry.getValue());
        List<Interaccion> interacciones = toEntities(interaccionExcels);

        int existentes = 0;
        for(Interaccion interaccion: interacciones){
            if(interaccion.getId() > 0)
                existentes++;
        }
        Map<String, Object> toRet = new HashMap<>();

        toRet.put("nuevos",interacciones.size() - existentes );
        toRet.put("actualizados", existentes);
        // Return the token on the response
        return Response.ok().entity(toRet).build();
    }
    public List<Interaccion> toEntities(List<InteraccionExcel> interaccionExcels){
        List<Interaccion> interacciones = new ArrayList<>();
        for(InteraccionExcel interaccionExcel: interaccionExcels){
            interacciones.add(toEntity(interaccionExcel));
        }
        return interacciones;
    }

    public Interaccion toEntity(InteraccionExcel excel){
        /*
        Si no se encuentra uno de los dos activos o la severidad, entonces primero se deben cargar éstos.
        De esta manera se evita el error de tipeo.
        Tampoco se agrega automaticamente si no se encuentra justamente debido al posible error de tipeo.
        Ej. Si en la planilla por error se tipeo 'FUROSEMIDAS' entonces estaría creando interacciones falsas.
         */
        Activo activoA;
        activoA = activoLogic.getByNombre(excel.getActivoA());
        if(activoA == null){
            if(excel.getActivoA() == null)
                throw new IllegalArgumentException("La celda de la columna " + ExcelInteraccion.ACTIVO_A +
                        " no puede estar vacía");
            else
                throw new IllegalArgumentException("No se encontró el activo: \"" + excel.getActivoA() + "\". " +
                        "Verifique que el activo se encuentre en el listado de activos del sistema. De lo contrario, agréguelo");
            /*activoA = new Activo();
            activoA.setNombre(excel.getActivoA());
            activoA = activoLogic.add(activoA);*/
        }

        Activo activoB;
        activoB = activoLogic.getByNombre(excel.getActivoB());
        if(activoB == null){
            if(excel.getActivoB() == null)
                throw new IllegalArgumentException("La celda de la columna " + ExcelInteraccion.ACTIVO_B +
                        " no puede estar vacía");
            else
                throw new IllegalArgumentException("No se encontró el activo: \"" + excel.getActivoB() + "\". " +
                        "Verifique que el activo se encuentre en el listado de activos del sistema. De lo contrario, agréguelo");
            /*activoB = new Activo();
            activoB.setNombre(excel.getActivoB());
            activoB = activoLogic.add(activoB);*/

        }

        Severidad severidad;
        severidad = severidadLogic.getByNombre(excel.getSeveridad());
        if(severidad == null){
            throw new IllegalArgumentException("No se encontró la severidad: \"" + excel.getSeveridad() + "\". " +
                    "Verifique que la severidad se encuentre en el listado de severidades del sistema. De lo contrario, agréguelo");
            /*severidad = new Severidad();
            severidad.setNombre(excel.getSeveridad());*/
        }

        Interaccion interaccion;
        interaccion = getByActivos(activoA.getId(), activoB.getId());
        if(interaccion == null){
            interaccion = new Interaccion();
            interaccion.setActivoA(activoA);
            interaccion.setActivoB(activoB);
        }

        interaccion.setSeveridad(severidad);
        interaccion.setEfecto(excel.getEfecto());
        add(interaccion);

        return interaccion;
    }
    

}