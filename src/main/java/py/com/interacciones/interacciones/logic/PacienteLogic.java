package py.com.interacciones.interacciones.logic;

import org.apache.commons.lang3.Validate;
import py.com.interacciones.interacciones.domain.*;

import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.ws.rs.core.Response;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.*;

@Stateless
public class PacienteLogic extends BaseLogic<Paciente> {
    //@formatter:off
    private static final String QUERY = ""
            + " SELECT e"
            + " FROM Paciente e"
            + " WHERE e.codigo = :codigo";
    //@formatter:on

    public Paciente getByCodigo(Long codigo) {
        Validate.notNull(codigo, "No se puede buscar por código nulo");
        try {
            return getEm().createQuery(QUERY, Paciente.class).setParameter("codigo", codigo)
                .getSingleResult();
        }catch (Exception ex){
            return null;
        }
    }

    public List<InteraccionPaciente> getInteraccionesPaciente(Long idPaciente, Long idMedicamento, Long idActivo,
                                                      String via, Boolean riesgo, Boolean controlado,
                                                      String fechaDesde, String fechaHasta){
        try {

            String QUERY_INTERACCIONES = getQueryString(idPaciente, idMedicamento, idActivo, via, riesgo, controlado);
            Query query= getEm().createNativeQuery(QUERY_INTERACCIONES);
            query.setParameter("fecha_desde", fechaDesde);
            query.setParameter("fecha_hasta", fechaHasta);
            if(idPaciente != null)
                query.setParameter("paciente", idPaciente);
            if(idMedicamento != null)
                query.setParameter("medicamento", idMedicamento);
            if(idActivo != null)
                query.setParameter("activo", idActivo);
            if(via != null)
                query.setParameter("via", via);
            if(controlado != null)
                query.setParameter("controlado", controlado);
            if(riesgo != null)
                query.setParameter("riesgo", riesgo);
            List<Object[]> resultado = query.getResultList();

            List<InteraccionPaciente> interacciones = new ArrayList<>();
            InteraccionPaciente interaccion;
            for(Object[] ob: resultado){
                interaccion = new InteraccionPaciente();
                interaccion.setIdPaciente((BigInteger) ob [0]);
                interaccion.setPaciente((String)ob[1]);
                interaccion.setCodigoPaciente((BigInteger)ob[2]);
                interaccion.setActivos((String)ob[3]);
                Timestamp fecha = (Timestamp) ob[4];
                interaccion.setFecha(new Date(fecha.getTime()));
                interaccion.setIdActivoA((BigInteger) ob[5]);
                interaccion.setActivoA((String) ob[6]);
                interaccion.setIdActivoB((BigInteger) ob[7]);
                interaccion.setActivoB((String) ob[8]);
                interaccion.setIdInteraccion((BigInteger) ob[9]);
                interaccion.setSeveridad((String) ob[10]);
                interaccion.setIdSeveridad((BigInteger) ob[11]);

                interacciones.add(interaccion);
            }
            return  interacciones;

        }catch (Exception ex){
            return null;
        }
    }

    public String getQueryString(Long idPaciente, Long idMedicamento, Long idActivo,
                                 String via, Boolean riesgo, Boolean controlado){
        String QUERY_INTERACCIONES = "SELECT " +
                "distinct paciente_ret as idpaciente, " +
                "p.nombre as paciente, " +
                "p.codigo as codigopaciente, " +
                "a1.nombre || ' + ' || a2.nombre as activos, " +
                "fecha_ret as fecha, " +
                "a1.id as activo1_codigo, " +
                "a1.nombre as activo1_nombre, " +
                "a2.id as activo2_codigo, " +
                "a2.nombre as activo2_nombre, " +
                "interaccion_ret, " +
                "s.nombre, " +
                "s.id " +
                "from obtener_interacciones(:fecha_desde, " +
                ":fecha_hasta, " +
                (idPaciente == null ? "null" : ":paciente") + ", " +
                (idMedicamento == null ?"null" : ":medicamento")+ ", " +
                (idActivo == null ?"null" : ":activo")+ ", " +
                (via == null ?"null" : ":via")+ ", " +
                (controlado == null ?"null" : ":controlado")+ ", " +
                (riesgo == null ?"null" : ":riesgo")+
                ") " +
                "join paciente p on p.id = paciente_ret " +
                "join interaccion i on i.id = interaccion_ret " +
                "join activo a1 on a1.id = i.activo_a " +
                "join activo a2 on a2.id = i.activo_b " +
                "join severidad s on s.id = i.severidad " +
                "order by p.nombre";
        return QUERY_INTERACCIONES;
    }

    public Response getInteracciones(Long idPaciente, Long idMedicamento, Long idActivo,
                                     String via, Boolean riesgo, Boolean controlado,
                                     String fechaDesde, String fechaHasta){

        List<InteraccionPaciente> interacciones = getInteraccionesPaciente(idPaciente, idMedicamento, idActivo,
                via, riesgo, controlado, fechaDesde, fechaHasta);

        Map<BigInteger, Frecuencia> masFrecuentes = new HashMap<>();
        Map<BigInteger, Frecuencia> activoMasFrecuentes = new HashMap<>();
        Map<BigInteger, Frecuencia> severidadMasFrecuentes = new HashMap<>();
        Map<BigInteger, Frecuencia> pacienteMasFrecuentes = new HashMap<>();

        for(InteraccionPaciente interaccion: interacciones){
            //Calculo del par de interacciones mas frecuentes
            calcularMasFrecuentes(masFrecuentes, interaccion);

            //Calculo del activo mas frecuente
            calcularActivosMasFrecuentes(activoMasFrecuentes, interaccion);

            //Calculo segun severidad
            calcularSeveridadMasFrecuentes(severidadMasFrecuentes, interaccion);

            //Calculo segun paciente
            calcularPacienteMasFrecuentes(pacienteMasFrecuentes, interaccion);

        }
        Map<String, Object> toRet = new HashMap<>();
        toRet.put("interacciones",interacciones);
        toRet.put("total",interacciones.size());
        toRet.put("masFrecuentes",masFrecuentes.values().stream().sorted((f1, f2) -> Integer.compare(f2.getCantidad(), f1.getCantidad())).toArray());
        toRet.put("activosMasFrecuentes",activoMasFrecuentes.values().stream().sorted((f1, f2) -> Integer.compare(f2.getCantidad(), f1.getCantidad())).toArray());
        toRet.put("severidadMasFrecuentes",severidadMasFrecuentes.values().stream().sorted((f1, f2) -> Integer.compare(f2.getCantidad(), f1.getCantidad())).toArray());
        toRet.put("pacienteMasFrecuentes",pacienteMasFrecuentes.values().stream().sorted((f1, f2) -> Integer.compare(f2.getCantidad(), f1.getCantidad())).toArray());
        // Return the token on the response
        return Response.ok().entity(toRet).build();
    }

    public void calcularPacienteMasFrecuentes(Map<BigInteger, Frecuencia> masFrecuentes,
                                      InteraccionPaciente interaccion){
        if(masFrecuentes.containsKey(interaccion.getIdPaciente())){
            Frecuencia masFrecuenteExistente = masFrecuentes.get(interaccion.getIdPaciente());
            masFrecuenteExistente.setCantidad(masFrecuenteExistente.getCantidad() + 1);
        }else{
            Frecuencia masFrecuente;
            masFrecuente =  new Frecuencia();
            masFrecuente.setCantidad(1);
            masFrecuente.setNombre(interaccion.getPaciente());
            masFrecuente.setCodigo(interaccion.getCodigoPaciente());
            masFrecuentes.put(interaccion.getIdPaciente(), masFrecuente);
        }
    }

    public void calcularMasFrecuentes(Map<BigInteger, Frecuencia> masFrecuentes,
                                      InteraccionPaciente interaccion){
        if(masFrecuentes.containsKey(interaccion.getIdInteraccion())){
            Frecuencia masFrecuenteExistente = masFrecuentes.get(interaccion.getIdInteraccion());
            masFrecuenteExistente.setCantidad(masFrecuenteExistente.getCantidad() + 1);
        }else{
            Frecuencia masFrecuente;
            masFrecuente =  new Frecuencia();
            masFrecuente.setCantidad(1);
            masFrecuente.setNombre(interaccion.getActivos());
            masFrecuentes.put(interaccion.getIdInteraccion(), masFrecuente);
        }
    }

    public void calcularActivosMasFrecuentes(Map<BigInteger, Frecuencia> activoMasFrecuentes,
                                             InteraccionPaciente interaccion){
        Frecuencia activoMasFrecuente;
        if(activoMasFrecuentes.containsKey(interaccion.getIdActivoA())){
            Frecuencia activoExistente = activoMasFrecuentes.get(interaccion.getIdActivoA());
            activoExistente.setCantidad(activoExistente.getCantidad() + 1);
        }else{
            activoMasFrecuente =  new Frecuencia();
            activoMasFrecuente.setCantidad(1);
            activoMasFrecuente.setNombre(interaccion.getActivoA());
            activoMasFrecuentes.put(interaccion.getIdActivoA(), activoMasFrecuente);
        }
        if(activoMasFrecuentes.containsKey(interaccion.getIdActivoB())){
            Frecuencia activoExistente = activoMasFrecuentes.get(interaccion.getIdActivoB());
            activoExistente.setCantidad(activoExistente.getCantidad() + 1);
        }else{
            activoMasFrecuente =  new Frecuencia();
            activoMasFrecuente.setCantidad(1);
            activoMasFrecuente.setNombre(interaccion.getActivoB());
            activoMasFrecuentes.put(interaccion.getIdActivoB(), activoMasFrecuente);
        }
    }

    public void calcularSeveridadMasFrecuentes(Map<BigInteger, Frecuencia> masFrecuentes,
                                      InteraccionPaciente interaccion){
        if(masFrecuentes.containsKey(interaccion.getIdSeveridad())){
            Frecuencia masFrecuenteExistente = masFrecuentes.get(interaccion.getIdSeveridad());
            masFrecuenteExistente.setCantidad(masFrecuenteExistente.getCantidad() + 1);
        }else{
            Frecuencia masFrecuente;
            masFrecuente =  new Frecuencia();
            masFrecuente.setCantidad(1);
            masFrecuente.setNombre(interaccion.getSeveridad());
            masFrecuentes.put(interaccion.getIdSeveridad(), masFrecuente);
        }
    }
}