package py.com.interacciones.interacciones.logic;

import org.apache.commons.lang3.Validate;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import py.com.interacciones.interacciones.domain.Medicamento;
import py.com.interacciones.interacciones.domain.Paciente;
import py.com.interacciones.interacciones.domain.Salida;
import py.com.interacciones.interacciones.domain.SalidaExcel;
import py.com.interacciones.framework.util.Excel;
import py.com.interacciones.framework.util.Utils;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;
import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Stateless
public class SalidaLogic extends BaseLogic<Salida> {

	@Inject
	PacienteLogic pacienteLogic;

	@Inject
	MedicamentoLogic medicamentoLogic;

	@Override
	public Salida add(Salida entity) {
		return super.add(entity);
	}
	@Override
	public Salida update(Salida entity) {
		return super.update(entity);
	}

	//@formatter:off
	private static final String QUERY = ""
			+ " SELECT e"
			+ " FROM Salida e"
			+ " WHERE e.codigo = :codigo";
	//@formatter:on
	//@formatter:off
	private static final String QUERY_ELIMINAR_RANGO = ""
			+ "delete from salidas where date(fecha) between date(:desde) and date(:hasta)";
	//@formatter:on

	public Salida getByCodigo(Long codigo) {
		Validate.notNull(codigo, "No se puede buscar por código nulo");
		try {
			return getEm().createQuery(QUERY, Salida.class).setParameter("codigo", codigo)
					.getResultList().get(0);
		}catch (Exception ex){
			return null;
		}
	}

	public Response subirExcel(MultipartFormDataInput input) throws Exception{
		HashMap<String, InputStream> map = Utils.getData(input);
		Excel excel = new Excel();
		Map.Entry<String, InputStream> entry = map.entrySet().iterator().next();

		List<SalidaExcel> salidaExcels = excel.obtenerDelExcel(entry.getKey(), entry.getValue());
		List<Salida> salidas = toEntities(salidaExcels);

		int existentes = 0;
		for(Salida salida: salidas){
			if(salida.getId() > 0)
				existentes++;
			try{
				add(salida);
			}catch (Exception ex){
				System.out.println();
			}
		}
		Map<String, Object> toRet = new HashMap<>();

		toRet.put("nuevos",salidas.size() - existentes );
		toRet.put("actualizados", existentes);
		// Return the token on the response
		return Response.ok().entity(toRet).build();
	}

	public Salida toEntity(SalidaExcel excel){
		Validate.notNull(excel.getCodigo(), "El campo " + Excel.SALIDA_ID +
				" no puede estar vacío. Fila: " + excel.getFila() );
		Validate.notNull(excel.getPaciente(), "El campo " + Excel.PACIENTE_ID +
				" no puede estar vacío. Fila: " + excel.getFila() );
		Validate.notNull(excel.getMedicamento(), "El campo " + Excel.MEDICAMENTO_ID +
				" no puede estar vacío. Fila: " + excel.getFila() );
		Validate.notNull(excel.getFecha(), "El campo " + Excel.FECHA +
				" no puede estar vacío. Fila: " + excel.getFila() );
		Validate.notNull(excel.getCantidad(), "El campo " + Excel.CANTIDAD +
				" no puede estar vacío. Fila: " + excel.getFila() );

		Salida salida;
		salida = getByCodigo(excel.getCodigo());
		if(salida == null){
			salida = new Salida();
			salida.setCodigo(excel.getCodigo());
			salida.setFecha(excel.getFecha());
			salida.setCantidad(excel.getCantidad());
		}

		Paciente paciente;
		paciente = pacienteLogic.getByCodigo(excel.getPaciente());
		if(paciente == null){
			paciente = new Paciente();
			paciente.setCodigo(excel.getPaciente());
			paciente.setNombre(excel.getPacienteNombre() == null? "": excel.getPacienteNombre() );
			paciente = pacienteLogic.add(paciente);
		}else{//Si ya encuentra actualiza los datos
			if(excel.getPacienteNombre() != null && !excel.getPacienteNombre().trim().equals(""))
				paciente.setNombre(excel.getPacienteNombre());
			paciente = pacienteLogic.update(paciente);
		}

		salida.setPaciente(paciente);
		Medicamento medicamento;
		medicamento = medicamentoLogic.getByCodigo(excel.getMedicamento());
		if(medicamento == null){
			medicamento = new Medicamento();
			medicamento.setCodigo(excel.getMedicamento());
			//Actualiza el nombre del medicamento solo si se encuentra en la celda
			medicamento.setNombre(excel.getMedicamentoNombre() == null? "": excel.getMedicamentoNombre());
			medicamento = medicamentoLogic.add(medicamento);
		}else{//Si ya encuentra actualiza los datos
			/*medicamento.setNombre(excel.getMedicamentoNombre());
			if(excel.getMedicamentoNombre() != null && !excel.getMedicamentoNombre().trim().equals(""))
				medicamento = medicamentoLogic.update(medicamento);*/
		}
		salida.setMedicamento(medicamento);
		salida.setFecha(excel.getFecha());
		salida.setCantidad(excel.getCantidad());
		return salida;
	}

	public List<Salida> toEntities(List<SalidaExcel> salidaExcels){
		List<Salida> salidas = new ArrayList<>();
		for(SalidaExcel salidaExcel: salidaExcels){
			salidas.add(toEntity(salidaExcel));
		}
		return salidas;
	}

	public int eliminarRango(String desde, String hasta){
		Validate.notNull(desde, "Debe especificar desde que fecha");
		Validate.notNull(hasta, "Debe especificar hasta que fecha");
		try {
			Query query = getEm().createNativeQuery(QUERY_ELIMINAR_RANGO);
			query.setParameter("desde", desde);
			query.setParameter("hasta", hasta);
			return query.executeUpdate();
		}catch (Exception ex){
			return 0;
		}
	}
}