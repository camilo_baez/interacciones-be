package py.com.interacciones.interacciones.logic;

import static org.torpedoquery.jpa.Torpedo.from;
import static org.torpedoquery.jpa.Torpedo.select;
import static org.torpedoquery.jpa.Torpedo.where;
import static org.torpedoquery.jpa.TorpedoFunction.count;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang.Validate;
import org.hibernate.Hibernate;
import org.jinq.tuples.Pair;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.collect.Iterables;

import py.com.interacciones.interacciones.domain.Permiso;
import py.com.interacciones.interacciones.domain.Rol;
import py.com.interacciones.interacciones.domain.Usuario;
import py.com.interacciones.interacciones.domain.Usuario.Estado;
import py.com.interacciones.framework.security.SessionTokenInfo;
import py.com.interacciones.framework.security.UserSessionDataHolder;

@Stateless
public class UsuarioLogic extends BaseLogic<Usuario> {

	public static final String PERMISSION_TO_CHANGE_ANY_PASS = "CHANGE_ANY_USER_PASSWORD";

	//@formatter:off
	public static final String QUERY_HAS_PERMISSION = ""
			+ " SELECT p"
			+ " FROM "
			+ " 		Usuario u"
			+ " 		join u.roles r"
			+ " 		join r.permisos p"
			+ " WHERE "
			+ " 		p.nombre = :name "
			+ " 	AND u = :user"
			+ " 	AND u.estado = 'ACTIVO'";
	//@formatter:on

	//@formatter:off
	public static final String QUERY_USERS_WITH_PERMISSION = ""
			+ " SELECT u"
			+ " FROM "
			+ " 		Usuario u"
			+ " 		join u.roles r"
			+ " 		join r.permisos p"
			+ " WHERE "
			+ " 		p.nombre = :name "
			+ " 	AND u.estado = 'ACTIVO'"
			+ " GROUP BY u";
	//@formatter:on

	@Inject
	private RolLogic rolLogic;

	@Inject
	private UserSessionDataHolder dataHolder;

	@Inject
	private SessionTokenInfo tokenInfo;

	/**
	 * Cambia la contraseña actual por su version en MD5 y luego persiste la
	 * entidad.
	 */
	@Override
	public Usuario add(Usuario entity) {
		Validate.notNull(entity.getTipoDocumento(), "El tipo de documento no puede ser vacío.");
		checkUnicoNombre(entity);
		entity.setEstado(Estado.ACTIVO);
		entity.setContrasenha(MD5(entity.getContrasenha()));
		return super.add(entity);

	}

	private void checkUnicoNombre(Usuario entity) {

		Usuario u = from(Usuario.class);
		where(u.getUsername()).eq(entity.getUsername());
		Long repetead = select(count(u)).get(getEm());

		Validate.isTrue(repetead < 1, "El nombre de usuario ya existe");
	}

	@Override
	public Usuario remove(long key) {

		return remove(findById(key));
	}

	@Override
	public Usuario remove(Usuario entity) {
		entity.setEstado(Usuario.Estado.INACTIVO);
		return getEm().merge(entity);
	}

	/**
	 * Verifica si la contraseña ya es un MD5, si no lo es, lo crea como md5
	 */
	@Override
	public Usuario update(Usuario entity) {
		// FIX-ME no cambiar contraseña por este método
		Usuario aCambiar = findById(entity.getId());
		aCambiar.setNombre(entity.getNombre());
		aCambiar.setDireccion(entity.getDireccion());
		aCambiar.setTelefono(entity.getTelefono());
		aCambiar.setCorreo(entity.getCorreo());
		aCambiar.setDocumento(entity.getDocumento());
		aCambiar.setTipoDocumento(entity.getTipoDocumento());
		aCambiar.setRoles(entity.getRoles());
		aCambiar.setConfiguraciones(entity.getConfiguraciones());
		return super.update(aCambiar);
	}

	/**
	 * Busca un usuario cuyo nombre de usuario sea <code>user</code> y cuya
	 * contraseña sea <code>password</code>.
	 * 
	 * @param user
	 *            nombre de usuario, no nulo.
	 * @param password
	 *            password, no nulo.
	 * @return Usuario que se asocia con el nombre y la constraña, nunca
	 *         <code>null</code>.
	 * @throws UserOrPasswordMismatchException
	 *             si el usuario y la contraseña no coincide con ningún usuario.
	 */
	public Usuario login(String user, String password) throws UserOrPasswordMismatchException {

		Objects.requireNonNull(user, "Usuario nulo");
		Objects.requireNonNull(password, "password nulo");

		String md5 = MD5(password);
		Usuario from = from(Usuario.class);
		where(from.getUsername()).eq(user).and(from.getContrasenha()).eq(md5).and(from.getEstado()).eq(Estado.ACTIVO);

		List<Usuario> result = select(from).list(getEm());

		if (result.isEmpty())
			throw new UserOrPasswordMismatchException();
		Usuario toRet = result.get(0);
		toRet.getRoles().size();
		return toRet;

	}

	/**
	 * Retorna true si el usuario existe.
	 * 
	 * @param userId
	 * @param password
	 * @return
	 */
	public boolean checkPassword(Usuario user, String password) {

		String md5 = MD5(password);
		Usuario from = from(Usuario.class);
		where(from.getId()).eq(user.getId()).and(from.getContrasenha()).eq(md5).and(from.getEstado())
				.eq(Usuario.Estado.ACTIVO);
		List<Long> result = select(from.getId()).list(getEm());
		return !result.isEmpty();
	}

	/**
	 * Retorna todos los nombres de usuarios
	 * 
	 * @return
	 */
	public List<Usuario> getNombreDeUsuarios(String nombre) {

		String realName;
		if (nombre == null)
			realName = "";
		else
			realName = nombre.toLowerCase();

		return getStreamAll().where(e -> e.getNombre().toLowerCase().contains(realName.toLowerCase()))
				.select(u -> new Pair<>(u.getId(), u.getNombre())).map(UsuarioLogic::build)
				.limit(getConfig().getLimiteAutoComplete()).collect(Collectors.toList());
	}

	private static Usuario build(Pair<Long, String> data) {
		Usuario toRet = new Usuario();
		toRet.setId(data.getOne());
		toRet.setNombre(data.getTwo());
		return toRet;
	}

	/**
	 * Retorna el hash MD5 de una cadena.
	 * 
	 * @param md5
	 *            cadena no nula.
	 * @return la cadena en formato md5
	 */
	private String MD5(String md5) {
		try {
			java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
			byte[] array = md.digest(md5.getBytes());
			StringBuffer sb = new StringBuffer();
			for (byte element : array) {
				sb.append(Integer.toHexString((element & 0xFF) | 0x100).substring(1, 3));
			}
			return sb.toString();
		} catch (java.security.NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Actualiza la contraseña de un usuario.
	 * 
	 * Si removeChangePass es true, el usuario no deberá cambiar su contraseña
	 * en el siguente inicio de sesión.
	 */
	public void updatePass(Usuario user, String newPass, boolean removeChangePass) {

		Usuario user2 = findById(user.getId());
		user2.setCambiarContrasenha(!removeChangePass);
		user2.setContrasenha(MD5(newPass));
		update(user2);
	}

	/**
	 * Verifica si un usuario tiene un permiso.
	 * 
	 * <p>
	 * Este metodo trata de no hacer llamadas innecsarias, por eso de su
	 * complejidad, por ejemplo, si el user pasado tiene los roles cargados, no
	 * se haran peticiones adicionales.
	 * </p>
	 * 
	 * @param user
	 *            usuario no nulo al cual buscar permiso
	 * @param permission
	 *            permiso deseado
	 * @return <code>true</code> si el usuario tiene permiso, <code>false</code>
	 *         en caso contrario.
	 */
	public boolean hasPermission(Usuario user, String permission) {

		if (user.getEstado() != Estado.ACTIVO)
			return false;
		if (!Hibernate.isInitialized(user.getRoles())) {
			// si no tiene los roles cargados, checkear por el rol especifico
			// con una consulta
			return getPermisoDeUsuario(user, permission) != null;
		}

		List<Rol> roles = user.getRoles();
		for (Rol r : roles) {

			List<Permiso> permisos = r.getPermisos();

			// veo si se han obtenido ya los permisos
			if (!Hibernate.isInitialized(user.getRoles())) {
				// si un rol no tiene cargados los permisos, vamos directos por
				// el permiso
				// es mejor que cargar todos los permisos de este rol.
				return getPermisoDeUsuario(user, permission) != null;
			}

			for (Permiso p : permisos) {
				if (p.getNombre().equals(permission)) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * Verifica si el usuario actual tiene un permiso dado.
	 * 
	 * @param string
	 * @return
	 */
	public boolean hasPermission(String string) {
		return hasPermission(dataHolder.getUser(), string);
	}

	private Permiso getPermisoDeUsuario(Usuario u, String name) {
		List<Permiso> fromDb = getEm().createQuery(QUERY_HAS_PERMISSION, Permiso.class).setParameter("name", name)
				.setParameter("user", u).getResultList();
		return Iterables.getFirst(fromDb, null);
	}

	public List<Usuario> getUsuariosWithPermission(String name) {
		return getEm().createQuery(QUERY_USERS_WITH_PERMISSION, Usuario.class).setParameter("name", name)
				.getResultList();
	}

	public void makeChangePass(Usuario u) {
		Usuario user = findById(u.getId());
		user.setCambiarContrasenha(true);
		update(user);
		tokenInfo.logout(u);
	}

	public Usuario getWithRoles(long key) {
		Usuario toRet = findById(key);
		toRet.getRoles().size();
		return toRet;
	}

	public Usuario updateConfig(Long id, JsonNode config) {
		Usuario u = findById(id);
		u.setConfiguraciones(config);
		return u;
	}

	public Usuario changePass(Long id, String oldPass, String newPass, String repeatNewPass) {

		Usuario u = findById(id);

		// SI es un usuario privilegiado, entonces se debe cambiar todo ya.
		if (hasPermission(dataHolder.getUser(), PERMISSION_TO_CHANGE_ANY_PASS)) {
			updatePass(u, newPass, true);
			return u;
		}

		// Si el usuario que se solicita el cambio no es el logueado, error.
		Validate.isTrue(id.equals(dataHolder.getUser().getId()), "No se puede cambiar la contraseña de otro usuario");
		// Si las contraseñas no coinciden, error
		Validate.isTrue(newPass.equals(repeatNewPass), "Las contraseñas no coinciden");

		try {
			login(u.getUsername(), oldPass);
		} catch (UserOrPasswordMismatchException uopme) {
			throw new IllegalArgumentException("La contraseña antigua no es correcta");
		}

		updatePass(u, newPass, true);
		return u;
	}

	/**
	 * Obliga al usuario a cambiar su password.
	 * 
	 * @param id
	 * @return
	 */
	public Usuario makeChangePass(Long id) {
		Usuario u = findById(id);
		makeChangePass(u);
		return u;
	}

	public Usuario getGenerador() {
		return getStreamAll().where(usuario -> usuario.getNombre() == "GENERADOR FACTURAS").getOnlyValue();
	}

	public List<Usuario> getWithGroup() {
		List<Usuario> resultList = getAll();
		return resultList;
	}

}
