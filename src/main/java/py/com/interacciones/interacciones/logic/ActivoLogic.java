package py.com.interacciones.interacciones.logic;

import org.apache.commons.lang3.Validate;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import py.com.interacciones.interacciones.domain.*;
import py.com.interacciones.framework.util.Config;
import py.com.interacciones.framework.util.ExcelActivo;
import py.com.interacciones.framework.util.Utils;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Stateless
public class ActivoLogic extends BaseLogic<Activo> {

	@Inject
	Config config;
	
	@Override
    public Activo update(Activo entity) {
		if(entity.getNombre() != null)
			entity.setNombre(entity.getNombre().trim());
		return super.update(entity);
	}
	@Override
	public Activo add(Activo entity) {
		if(entity.getNombre() != null)
			entity.setNombre(entity.getNombre().trim());
		return super.add(entity);
	}

	//@formatter:off
	private static final String QUERY = ""
			+ " SELECT e"
			+ " FROM Activo e"
			+ " WHERE e.nombre = :nombre";
	//@formatter:on
	public Activo getByNombre(String nombre) {
		try {
			Validate.notNull(nombre, "No se puede buscar por nombre vacío");
			return getEm().createQuery(QUERY, Activo.class).setParameter("nombre", nombre)
					.getResultList().get(0);
		}catch (Exception ex){
			return null;
		}
	}
	public Response subirExcel(MultipartFormDataInput input) throws Exception{
		HashMap<String, InputStream> map = Utils.getData(input);
		ExcelActivo excel = new ExcelActivo();
		Map.Entry<String, InputStream> entry = map.entrySet().iterator().next();
		List<ActivoExcel> activoExcels = excel.obtenerDelExcel(entry.getKey(), entry.getValue());
		List<Activo> activos = toEntities(activoExcels);

		int existentes = 0;
		for(Activo activo: activos){
			if(activo.getId() > 0)
				existentes++;
			try{
				add(activo);
			}catch (Exception ex){
				System.out.println();
			}
		}
		Map<String, Object> toRet = new HashMap<>();

		toRet.put("nuevos",activos.size() - existentes );
		toRet.put("actualizados", existentes);
		// Return the token on the response
		return Response.ok().entity(toRet).build();
	}
	public List<Activo> toEntities(List<ActivoExcel> activoExcels){
		List<Activo> activos = new ArrayList<>();
		for(ActivoExcel activoExcel: activoExcels){
			activos.add(toEntity(activoExcel));
		}
		return activos;
	}

	public Activo toEntity(ActivoExcel excel){
        
		Activo activo;
		activo = getByNombre(excel.getActivo());
		if(activo == null){
			activo = new Activo();
			activo.setNombre(excel.getActivo());
		}
		activo.setDescripcion(excel.getDescripcion());

		return activo;
	}
}
