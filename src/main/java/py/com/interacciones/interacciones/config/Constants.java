package py.com.interacciones.interacciones.config;

/**
 * Created by sortiz on 6/13/16.
 */
public class Constants {
    public static final String NOMBRE_SANATORIO = "Sanatorio Christian S.R.L.";
    public static final String DIRECCION_SANATORIO = "Saturio Ríos y San Lorenzo";
    public static final String TELEFONO_SANATORIO = "583-058 / 576 331";
    public static final String FAX_SANATORIO = "576-330";
    public static final java.lang.String TITULO_DATOS_MEDICO = "Datos del Médico";

    public static final java.lang.String TITULO_RESUMEN_SESION_CAJA = "Resumen de Caja";
    public static final java.lang.String TITULO_TRASPASO_SESION_CAJA = "Traspaso de Caja";

    public static final java.lang.String TITULO_CIERRE_CAJA = "Cierre de Caja";

    public static final java.lang.String TITULO_REPORTE = "Reporte de ";
    public static final java.lang.String HEADER_TITULO_REPORTE_CONSULTAS = "Consultas";
    public static final java.lang.String HEADER_SUBTITULO_REPORTE_CONSULTAS_MEDICOS = "Informe por Médico";
    public static final java.lang.String HEADER_SUBTITULO_REPORTE_CONSULTAS_SEGUROS = "Informe por Seguro Médico";

    public static final java.lang.String HEADER_TITULO_REPORTE_SESION_CAJA = "Sesión Caja";
    public static final java.lang.String HEADER_SUBTITULO_REPORTE_SESION_CAJA = "Transacciones";


    public static final java.lang.String HEADER_COLUMN_MEDICO = "Médico";


    public static final java.lang.String LABEL_PERIODO = "Periodo: ";
    public static final java.lang.String LABEL_TELEFONO = "Tel.: ";
    public static final java.lang.String LABEL_TOTALES = "Totales: ";
    public static final java.lang.String LABEL_SEGURO =  "Seguro: ";


    public static final String TEMPLATE_HEADER_INFORME_CONSULTA = "<b>%(titulo):</b> %(subtitulo) / <i>%(param) - %(periodo)</i>";
    public static final String TEMPLATE_HEADER_INFORME_SIN_DETALLE = "<b>%(titulo):</b> %(subtitulo)";


    public static final String REPORT_FONT = "Arial";
    public static final String SIMBOLO_GUARANI = "Gs. ";

    public static final String REPORT_SESION_CAJA_NOMBRE = "SESION-CAJA_%d";
    public static final String TRANSACCIONES = "Transacciones";
    public static final String LABEL_MONTO_APERTURA = "Monto Apretura: ";
    public static final String LABEL_MONTO_CIERRE = "Monto Cierre: ";
    public static final String LABEL_MONTO_CIERRE_CAL = "Monto Cierre Calculado: ";
    public static final String LABEL_ABIERTA_POR = "Abierta por: ";
    public static final String LABEL_FECHA_APERTURA = "En Fecha: ";
    public static final String LABEL_FECHA_CIERRE = "Cerrado En Fecha: ";
    public static final String LABEL_UBICACION_CAJA = "Ubicación: ";
	public static final String LABEL_USUARIO_CIERRE = "Cerrado por: ";
    public static final String LABEL_USUARIO_TRASPASO = "Traspasado por - a: ";
    public static final String LABEL_FECHA_TRASPASO = "Traspadado En Fecha: ";
    public static final String LABEL_MONTO_TRASPASO = "Monto Cierre: ";
    public static final String LABEL_MONTO_TRASPASO_CAL = "Monto Cierre Calculado: ";
}
