package py.com.interacciones.interacciones.config;

import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import io.swagger.jaxrs.config.BeanConfig;
import py.com.interacciones.interacciones.services.*;
import py.com.interacciones.interacciones.services.report.LogProvider;
import py.com.interacciones.framework.security.AuthenticationEndpoint;
import py.com.interacciones.framework.security.AuthenticationFilter;

/**
 * @author Sebastian Ortiz
 * @since 13/10/2016.
 */
@ApplicationPath("/rest/")
public class RestApplication extends Application {

	HashSet<Object> singletons = new HashSet<>();

	public RestApplication() {
		BeanConfig beanConfig = new BeanConfig();
		beanConfig.setVersion("1.0");
		beanConfig.setSchemes(new String[] { "http" });
		beanConfig.setHost("localhost:8080");
		beanConfig.setBasePath("/interacciones/rest");
		beanConfig.setResourcePackage("py.com.interacciones.interacciones.services");
		beanConfig.setScan(true);

		// Cambia el idioma de la JVM
		Locale.setDefault(new Locale("es", "PY"));
	}

	@Override
	public Set<Class<?>> getClasses() {
		HashSet<Class<?>> set = new HashSet<>();

		set.add(ObjectMapperProvider.class);

		set.add(AuthenticationEndpoint.class);
		// Comentar esta linea para que no exista seguridad!
		set.add(AuthenticationFilter.class);
		set.add(UsuarioService.class);
		set.add(RolService.class);
		set.add(PermisoService.class);
		set.add(IllegalArgumentExceptionMapper.class);
		set.add(NullPointerExceptionMapper.class);
		set.add(IllegalStateExceptionMapper.class);
		set.add(EjbExceptionMapper.class);
		set.add(ValidationExceptionMapper.class);
		set.add(LogProvider.class);

		//Sistema de Interacciones
		set.add(SalidaService.class);
		set.add(PacienteService.class);
		set.add(ActivoService.class);
		set.add(MedicamentoService.class);
		set.add(InteraccionService.class);
		set.add(SeveridadService.class);
		set.add(ResultadosService.class);

		set.add(io.swagger.jaxrs.listing.ApiListingResource.class);
		set.add(io.swagger.jaxrs.listing.SwaggerSerializers.class);
		return set;
	}

	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}
}
