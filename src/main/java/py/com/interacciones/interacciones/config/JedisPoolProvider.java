package py.com.interacciones.interacciones.config;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import py.com.interacciones.framework.util.Config;
import redis.clients.jedis.JedisPool;

@ApplicationScoped
public class JedisPoolProvider {

	@Inject
	Config config;

	@Produces
	JedisPool jedisPool;

	@PostConstruct
	public void produceJedisPool() {
		String host = config.getString("jedis.url", "127.0.0.1");
		int port = config.getInteger("jedis.port", 6379);

		System.out.println("Connecting to redis, with values " + host + ":" + port);
		jedisPool = new JedisPool(host, port);
	}

}
