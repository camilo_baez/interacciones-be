package py.com.interacciones.framework.util;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import py.com.interacciones.interacciones.domain.ActivoExcel;
import py.com.interacciones.interacciones.logic.ActivoLogic;

import javax.inject.Inject;
import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by camilo on 08/08/17.
 */
public class ExcelActivo {
    private static final String FILE_NAME = "/opt/excel.xlsx";

    public static final String ACTIVO = "ACTIVO";
    public static final String DESCRIPCION = "DESCRIPCION";

    public static final int filaEncabezado = 0;
    int activo = -1;
    int descripcion = -1;

    @Inject
    ActivoLogic pacienteLogic;

    public List<ActivoExcel> obtenerDelExcel(String excelFilePath, InputStream inputStream) throws IOException {
        List<ActivoExcel> activos = new ArrayList<>();
        Workbook workbook = Utils.getWorkbook(inputStream, excelFilePath);
        Sheet firstSheet = workbook.getSheetAt(0);

        Iterator<Row> iterator = firstSheet.iterator();

        verificarEncabezado(iterator);

        //Los datos estan justamente debajo del encabezado
        while (iterator.hasNext()) {
            Row nextRow = iterator.next();
            ActivoExcel activoExcel = new ActivoExcel();

            activoExcel.setActivo((String) Utils.getCellValue(nextRow.getCell(activo)));
            if(descripcion != -1)
                activoExcel.setDescripcion((String) Utils.getCellValue(nextRow.getCell(descripcion)));

            activos.add(activoExcel);
        }

        workbook.close();
        inputStream.close();

        return activos;
    }

    private void verificarEncabezado(Iterator<Row> iterator) {
        Row nextRow = iterator.next();

        //Omite las filas anteriores al encabezado
        while (iterator.hasNext() && nextRow.getRowNum() < filaEncabezado) {
            nextRow = iterator.next();
        }

        Iterator<Cell> cellIterator = nextRow.cellIterator();
        boolean configurado = false;
        while (cellIterator.hasNext()) {
            Cell nextCell = cellIterator.next();
            setPositions(nextCell);
            //Si ya configuró todas las columnas, terminar
            if (-1 != activo
                    && -1 != descripcion) {
                configurado = true;
                break;
            }
        }

        if(!configurado){
            if(activo == -1){
                throw new IllegalArgumentException("Verificar encabezado: " + ACTIVO);
            }
        }
    }

    private void setPositions(Cell nextCell) {
        switch (nextCell.getStringCellValue()) {
            case ACTIVO:
                activo = nextCell.getColumnIndex();
                break;
            case DESCRIPCION:
                descripcion = nextCell.getColumnIndex();
                break;
        }
    }
}
