package py.com.interacciones.framework.util;

import java.awt.Color;
import java.text.FieldPosition;
import java.text.Format;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.function.Function;

import ar.com.fdvs.dj.domain.CustomExpression;
import ar.com.fdvs.dj.domain.DJCalculation;
import ar.com.fdvs.dj.domain.DJValueFormatter;
import ar.com.fdvs.dj.domain.Style;
import ar.com.fdvs.dj.domain.builders.ColumnBuilder;
import ar.com.fdvs.dj.domain.builders.FastReportBuilder;
import ar.com.fdvs.dj.domain.constants.Border;
import ar.com.fdvs.dj.domain.constants.Font;
import ar.com.fdvs.dj.domain.constants.HorizontalAlign;
import ar.com.fdvs.dj.domain.constants.VerticalAlign;
import ar.com.fdvs.dj.domain.entities.columns.AbstractColumn;
import py.com.interacciones.interacciones.config.Constants;

/**
 * Created by sortiz on 6/10/16.
 */
public class ReportUtil {

	@SuppressWarnings("rawtypes")
	public static AbstractColumn createCustomColumn(String title, Integer width, Style style,
			Function<Map, String> formatter) {
		AbstractColumn column = ColumnBuilder.getNew()
				.setTitle(title)
				.setWidth(width)
				.setStyle(style)
				.setCustomExpression(createExpresion(String.class, formatter))
				.build();
		return column;
	}

	@SuppressWarnings("rawtypes")
	public static AbstractColumn createCustomColumn(String title, Integer width, Style style, String field,
			Class clazz) {
		AbstractColumn column = ColumnBuilder.getNew()
				.setTitle(title)
				.setWidth(width)
				.setStyle(style)
				.setColumnProperty(field, clazz.getName())
				.build();
		return column;
	}

	@SuppressWarnings("rawtypes")
	public static AbstractColumn createCustomColumnAlignRight(String title, Integer width, Integer fontSize,
			Function<Map, String> formatter) {
		Style style;
		if (fontSize != null) {
			style = Estilos.alineadoDerecha(fontSize);
		} else {
			style = Estilos.alineadoDerecha();
		}
		return createCustomColumn(title, width, style, formatter);
	}

	@SuppressWarnings("rawtypes")
	public static AbstractColumn createCustomColumnAlignRight(String title, Integer width,
			Function<Map, String> formatter) {
		return createCustomColumnAlignRight(title, width, null, formatter);
	}

	@SuppressWarnings("rawtypes")
	public static AbstractColumn createCustomColumnAlignLeft(String title, Integer width, Integer fontSize,
			Function<Map, String> formatter) {
		Style style;
		if (fontSize != null) {
			style = Estilos.alineadoIzquierda(fontSize);
		} else {
			style = Estilos.alineadoIzquierda();
		}
		return createCustomColumn(title, width, style, formatter);
	}

	@SuppressWarnings("rawtypes")
	public static AbstractColumn createCustomColumnAlignLeft(String title, Integer width,
			Function<Map, String> formatter) {
		return createCustomColumnAlignLeft(title, width, null, formatter);
	}

	public static AbstractColumn createCustomColumnAlignRight(String title, Integer width, Integer fontSize,
			String field) {
		return createCustomColumn(title, width, Estilos.alineadoDerecha(fontSize), field, String.class);
	}

	@SuppressWarnings("rawtypes")
	public static AbstractColumn createCustomColumnAlignRight(String title, Integer width, Integer fontSize,
			String field, Class clazz) {
		return createCustomColumn(title, width, Estilos.alineadoDerecha(fontSize), field, clazz);
	}

	@SuppressWarnings("rawtypes")
	public static AbstractColumn createCustomColumnAlignRight(String title, Integer width, String field, Class clazz) {
		return createCustomColumn(title, width, Estilos.alineadoDerecha(), field, clazz);
	}

	@SuppressWarnings("rawtypes")
	public static AbstractColumn createCustomColumnAlignLeft(String title, Integer width, Integer fontSize,
			String field, Class clazz) {
		return createCustomColumn(title, width, Estilos.alineadoIzquierda(fontSize), field, clazz);
	}

	@SuppressWarnings("rawtypes")
	public static AbstractColumn createCustomColumnAlignLeft(String title, Integer width, String field, Class clazz) {
		return createCustomColumn(title, width, Estilos.alineadoIzquierda(), field, clazz);
	}

	public static AbstractColumn createCustomColumnGuaranies(String title, Integer width, Integer fontSize,
			String field) {
		return createCustomColumnAlignRight(title, width, fontSize, lambdaGuaranies(field));
	}

	public static AbstractColumn addCustomColumnGuaranies(FastReportBuilder frb, String title, Integer width,
			String field, Integer fontSize, Class<?> clazz) {
		return addCustomColumnGuaranies(frb, title, width, field, fontSize, clazz, false);
	}

	public static AbstractColumn addCustomColumnGuaranies(FastReportBuilder frb, String title, Integer width,
			String field, Integer fontSize, Class<?> clazz, boolean summarize) {
		AbstractColumn column = createCustomColumnGuaranies(title, width, field, fontSize, clazz);
		frb.addColumn(column);
		if (summarize) {
			frb.addGlobalFooterVariable(column,
					DJCalculation.SUM,
					Estilos.sumarizacionDerecha(fontSize),
					getDJNumberFormatter());
		}
		return column;
	}

	public static AbstractColumn createCustomColumnGuaranies(String title, Integer width, String field,
			Integer fontSize, Class<?> clazz) {
		Style style = fontSize != null ? Estilos.alineadoDerecha(fontSize) : Estilos.alineadoDerecha();
		AbstractColumn column = createCustomColumn(title, width, style, field, clazz);
		column.setTextFormatter(getNumberFormatter(false));
		column.setHeaderStyle(Estilos.titleRight());
		return column;
	}

	public static AbstractColumn createCustomColumnGuaranies(String title, Integer width, String field) {
		return createCustomColumnGuaranies(title, width, null, field);
	}

	public static AbstractColumn createCustomColumnFecha(String title, Integer width, Integer fontSize, String field) {
		return createCustomColumnAlignRight(title, width, fontSize, lambdaFecha(field));
	}

	public static AbstractColumn createCustomColumnFecha(String title, Integer width, String field) {
		return createCustomColumnFecha(title, width, null, field);
	}

	@SuppressWarnings("rawtypes")
	public static Function<Map, String> lambdaGuaranies(String field) {
		return (e -> ReportUtil.formatToGuaranies((Number) e.get(field)));
	}

	@SuppressWarnings("rawtypes")
	public static Function<Map, String> lambdaFecha(String field) {
		return (e -> ReportUtil.formatToFechaHora((Date) e.get(field)));
	}

	@SuppressWarnings("rawtypes")
	public static CustomExpression createExpresion(Class<?> clazz, Function<Map, String> formatter) {
		return new CustomExpression() {

			private static final long serialVersionUID = -3650775498730255727L;

			@Override
			public String getClassName() {
				return clazz.getName();
			}

			@Override
			public Object evaluate(Map fields, Map variables, Map parameters) {
				return formatter.apply(fields);
			}
		};
	}

	public static String formatToGuaranies(Number number) {
		return formatToGuaranies(number, true);
	}

	public static String formatToGuaranies(Number number, boolean addPrefix) {
		if (number != null) {
			Locale locale = new Locale("de", "DE");
			NumberFormat numberFormatter = NumberFormat.getNumberInstance(locale);
			if (addPrefix)
				return Constants.SIMBOLO_GUARANI + numberFormatter.format(number);
			else
				return numberFormatter.format(number);
		} else {
			return "-";
		}
	}

	/**
	 * Retorna un {@link Format} que permite SERIALIZAR numeros a su forma en
	 * guaranies.
	 */
	public static Format getNumberFormatter() {

		return getNumberFormatter(true);
	}

	/**
	 * Retorna un {@link Format} que permite SERIALIZAR numeros a su forma en
	 * guaranies.
	 */
	public static Format getNumberFormatter(boolean addPrefix) {

		return new Format() {
			private static final long serialVersionUID = 1L;

			@Override
			public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
				return toAppendTo.append(formatToGuaranies((Number) obj, addPrefix));
			}

			@Override
			public Object parseObject(String source, ParsePosition pos) {
				return null;
			}
		};
	}

	/**
	 * Retorna un {@link DJValueFormatter} que permite formatear numeros a su
	 * forma en guaranies.
	 */
	public static DJValueFormatter getDJNumberFormatter() {

		return new DJValueFormatter() {

			@Override
			public String getClassName() {
				return String.class.getName();
			}

			@Override
			@SuppressWarnings("rawtypes")
			public Object evaluate(Object value, Map fields, Map variables, Map parameters) {
				return formatToGuaranies((Number) value);
			}
		};
	}

	public static String formatToFechaHora(Date date) {
		return new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(date);
	}

	public static String formatToFecha(Date date) {
		return new SimpleDateFormat("dd/MM/yyyy").format(date);
	}

	public static class Estilos {

		public static Style normalDerecha() {
			Style normalDerecha = new Style();
			normalDerecha.setHorizontalAlign(HorizontalAlign.RIGHT);
			normalDerecha.setFont(Font.ARIAL_MEDIUM);
			return normalDerecha;
		}

		public static Style sumarizacionDerecha(Integer fontSze) {
			Style s = sumarizacionDerecha();
			s.setFont(new Font(fontSze, Constants.REPORT_FONT, true));
			s.setBorderTop(Border.PEN_1_POINT());
			return s;
		}

		public static Style normalDerechaBold() {
			Style normalDerecha = new Style();
			normalDerecha.setHorizontalAlign(HorizontalAlign.RIGHT);
			normalDerecha.setFont(Font.ARIAL_MEDIUM_BOLD);
			return normalDerecha;
		}

		public static Style normalIzquierda() {
			Style normalIzquierda = new Style();
			normalIzquierda.setHorizontalAlign(HorizontalAlign.LEFT);
			normalIzquierda.setFont(Font.ARIAL_MEDIUM);
			return normalIzquierda;
		}

		public static Style normalIzquierdaBold() {
			Style normalIzquierda = new Style();
			normalIzquierda.setHorizontalAlign(HorizontalAlign.LEFT);
			normalIzquierda.setFont(Font.ARIAL_MEDIUM_BOLD);
			return normalIzquierda;
		}

		public static Style alineadoDerecha() {
			Style alineadoDerecha = new Style();
			alineadoDerecha.setHorizontalAlign(HorizontalAlign.RIGHT);
			alineadoDerecha.setVerticalAlign(VerticalAlign.TOP);
			return alineadoDerecha;
		}

		public static Style alineadoDerecha(Integer fontSize) {
			Style alineadoDerecha = new Style();
			alineadoDerecha.setHorizontalAlign(HorizontalAlign.RIGHT);
			alineadoDerecha.setVerticalAlign(VerticalAlign.TOP);
			alineadoDerecha.setFont(new Font(fontSize, Constants.REPORT_FONT, false));
			return alineadoDerecha;
		}

		public static Style alineadoIzquierda() {
			Style alineadoIzquierda = new Style();
			alineadoIzquierda.setHorizontalAlign(HorizontalAlign.LEFT);
			alineadoIzquierda.setVerticalAlign(VerticalAlign.TOP);
			return alineadoIzquierda;
		}

		public static Style alineadoIzquierda(Integer fontSize) {
			Style alineadoIzquierda = new Style();
			alineadoIzquierda.setHorizontalAlign(HorizontalAlign.LEFT);
			alineadoIzquierda.setVerticalAlign(VerticalAlign.TOP);
			alineadoIzquierda.setFont(new Font(fontSize, Constants.REPORT_FONT, false));
			return alineadoIzquierda;
		}

		public static Style titleMiddle() {
			Style defaultTitle = new FastReportBuilder().build().getTitleStyle();
			defaultTitle.setVerticalAlign(VerticalAlign.MIDDLE);
			return defaultTitle;
		}

		public static Style titleBorderless() {
			Style titulo = new Style();
			titulo.setBackgroundColor(Color.white);
			titulo.setFont(new Font(12, Constants.REPORT_FONT, true));
			return titulo;
		}

		public static Style title() {
			Style titulo = new Style();

			titulo.setFont(new Font(10, Constants.REPORT_FONT, true));
			titulo.setBorderBottom(Border.PEN_1_POINT());
			titulo.setBorderTop(Border.PEN_1_POINT());
			titulo.setBackgroundColor(new Color(230, 230, 230, 128));
			titulo.setTransparent(false);
			titulo.setVerticalAlign(VerticalAlign.MIDDLE);
			return titulo;
		}

		public static Style titleLeft() {
			Style titulo = title();
			titulo.setHorizontalAlign(HorizontalAlign.LEFT);
			return titulo;
		}

		public static Style titleRight() {
			Style titulo = title();
			titulo.setHorizontalAlign(HorizontalAlign.RIGHT);
			return titulo;
		}

		public static Style alineadoDerechaConBorde() {
			Style alineadoDerecha = alineadoDerecha();
			alineadoDerecha.setFont(Font.ARIAL_MEDIUM_BOLD);
			alineadoDerecha.setBorder(Border.THIN());
			return alineadoDerecha;
		}

		public static Style oculto() {
			Style oculto = new Style();
			oculto.setTransparent(true);
			oculto.setTextColor(Color.white);
			oculto.setPadding(0);
			oculto.setBorder(Border.NO_BORDER());
			oculto.setFont(new Font(0, Constants.REPORT_FONT, false));
			return oculto;
		}

		public static Style firma() {
			Style oculto = oculto();
			oculto.setBorderBottom(Border.PEN_1_POINT());
			oculto.setFont(new Font(0, Constants.REPORT_FONT, false));
			return oculto;
		}

		public static Style subSumarizacion() {
			Style estiloTotal = new Style();
			estiloTotal.setFont(Font.ARIAL_MEDIUM_BOLD);
			estiloTotal.setBorderTop(Border.PEN_1_POINT());
			return estiloTotal;
		}

		public static Style subSumarizacionDerecha() {
			Style estiloTotal = subSumarizacion();
			estiloTotal.setHorizontalAlign(HorizontalAlign.RIGHT);
			return estiloTotal;
		}

		public static Style sumarizacionDerecha() {
			Style cabeza = new Style();
			cabeza.setFont(Font.ARIAL_MEDIUM_BOLD);
			cabeza.setBackgroundColor(Color.WHITE);
			cabeza.setHorizontalAlign(HorizontalAlign.RIGHT);
			return cabeza;
		}

		public static Style sumarizacionIzquierda() {
			Style cabezaLeft = new Style();
			cabezaLeft.setFont(Font.ARIAL_MEDIUM_BOLD);
			cabezaLeft.setBackgroundColor(Color.WHITE);
			cabezaLeft.setHorizontalAlign(HorizontalAlign.LEFT);
			return cabezaLeft;
		}
	}

	public static int pxToCm(Double cm) {
		Double pxPerCm = 37.8;
		Double result = cm * pxPerCm;
		return result.intValue();
	}
}
