package py.com.interacciones.framework.util;

import java.util.Optional;

import lombok.Value;

public class PrinterMargins {

	public Margin getMargins(String printerName, String type) {

		System.out.println("Margenes para impresora " + printerName);

		Double margenX = null;
		Double margenY = null;
		if ("EDGAR_LASER".equals(printerName)) {
			margenY = 1.0;
			switch (type) {
			case "FACTURA":
				margenX = 4.27;
				break;
			default:
				margenX = 4.27;
			}
		}

		if ("RECEPCION_LASER".equals(printerName)) {
			switch (type) {
			case "FACTURA":
				margenX = 4.27;
				break;
			default:
				margenX = 4.27;
			}
			margenY = 5.68;
		}

		return new Margin(Optional.ofNullable(margenX), Optional.ofNullable(margenY));
	}

	@Value
	public static class Margin {
		Optional<Double> x;
		Optional<Double> y;
	}
}
