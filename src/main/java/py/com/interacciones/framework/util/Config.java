package py.com.interacciones.framework.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.spi.CDI;

import org.apache.commons.lang3.Validate;

/**
 * Bean que facilita la confiravés de un archivo en el Sistema.
 *
 * @author sortiz
 *
 */
@ApplicationScoped
public class Config {

	/**
	 * Limita la cantidad de opciones que tiene el autocomplete.
	 */
	private static final Integer LIMIT_AUTO_COMPLETE = 5;
	
	private static final Integer LIMIT_DEPTH = 5;

	private static final String LOG_LOCATION = "/startup.log";

	/**
	 * Ubicaicón del servidor de impresion
	 */
	private static final String PRINTER_SERVER = "http://localhost:8082/cups/rest";
	private static final String PRINTER_USER = "admin";
	private static final String PRINTER_PASS = "21232f297a57a5a743894a0e4a801fc3";

	private static final String PROPERTY_FILE_LOCATION = "/";
	private static final String PROPERTY_FILE_NAME = "config.properties";

	private static final String FACTURA_DESCRIPCION_CONSULTA = "Consulta %s de %s con el doctor %s del paciente %s";
	private static final String FACTURA_DESCRIPCION_ESTUDIO = "Estudio %s de %s con el doctor %s del paciente %s";
	
	private static final String FILE_SERVER = "/home/sgde/Downloads/";

	private Properties properties;

	@PostConstruct
	void readFile() {

		properties = new Properties();
		String configurationAbsolutePath = PROPERTY_FILE_LOCATION + System.getProperty("file.separator")
				+ PROPERTY_FILE_NAME;
		try (InputStream is = new FileInputStream(configurationAbsolutePath)) {
			properties.load(is);
		} catch (IOException fnfe) {
			System.out.println(
					"No se puede obtener archivo de configuración con path absoluto " + configurationAbsolutePath);
			System.out.println(
					"No se puede obtener archivo de configuración con path absoluto " + configurationAbsolutePath);
			System.out.println(
					"No se puede obtener archivo de configuración con path absoluto " + configurationAbsolutePath);
			System.out.println(
					"No se puede obtener archivo de configuración con path absoluto " + configurationAbsolutePath);

			System.out.println("Se busca en resources del war");
			ClassLoader classloader = Thread.currentThread().getContextClassLoader();
			try (InputStream stream = classloader.getResourceAsStream(PROPERTY_FILE_NAME)) {
				properties.load(stream);
			} catch (IOException e) {
				System.out.println(
						"No se puede obtener archivo de configuración desde resources, utilizando valores por defecto");
			}
		}

	}

	private static Config INSTANCE;

	public static Config getStatic() {
		if (INSTANCE == null)
			INSTANCE = CDI.current().select(Config.class).get();
		return INSTANCE;
	}

	public Integer getLimiteAutoComplete() {
		return getInteger("rest.result.limit", LIMIT_AUTO_COMPLETE);
	}
	
	public Integer getLimitDepth() {
		return getInteger("actividad.profundidad", LIMIT_DEPTH);
	}

	public boolean isDevel() {
		return getBool("debug.enabled", false);
	}

	public String getLogLocation() {
		return properties.getProperty("log.location", LOG_LOCATION);
	}

	public String getPrinterServerURL() {
		return properties.getProperty("printer.url", PRINTER_SERVER);
	}

	public String getPrinterServerUser() {
		return properties.getProperty("printer.user", PRINTER_USER);
	}

	public String getPrinterServerPass() {
		return properties.getProperty("printer.pass", PRINTER_PASS);
	}

	public String getFacturaDescripcionConsulta() {
		return properties.getProperty("factura.consulta.descripcion", FACTURA_DESCRIPCION_CONSULTA);
	}

	public String getFacturaDescripcionEstudio() {
		return properties.getProperty("factura.estudio.descripcion", FACTURA_DESCRIPCION_ESTUDIO);
	}

	public Integer getInteger(String key, Integer value) {
		return properties.getProperty(key) != null ? Integer.valueOf(properties.getProperty(key)) : value;
	}

	public String getString(String key, String de) {
		String toRet = properties.getProperty(Validate.notNull(key, "No se puede buscar con clave nula"));
		return toRet == null ? de : toRet;
	}

	public Boolean getBool(String key, Boolean def) {
		return properties.getProperty(key) != null ? Boolean.valueOf(properties.getProperty(key)) : def;
	}

	public int getSessionTimeout() {
		return getInteger("session.timeout", 50000);
	}
	
	public String getFileServer(){
		return getString("fileserver.url", FILE_SERVER);
	}

}
