package py.com.interacciones.framework.util;

/**
 * Created by camilo on 25/08/17.
 */
import org.flywaydb.core.Flyway;

public class Migrations {
    public static void main(String[] args) throws Exception {
        Flyway flyway = new Flyway();
        String database = System.getenv("JDBC_DATABASE_URL");
        String user = System.getenv("JDBC_DATABASE_USERNAME");
        String pass = System.getenv("JDBC_DATABASE_PASSWORD");
        if(database == null || database.isEmpty()){
            database = "jdbc:postgresql://localhost:5432/interacciones";
            user = "postgres";
            pass = "postgres";
        }
        flyway.setDataSource(database, user, pass);
        flyway.migrate();
    }
}
