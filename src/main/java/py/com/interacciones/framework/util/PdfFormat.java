package py.com.interacciones.framework.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;

import net.sf.jasperreports.engine.design.JasperDesign;
import py.com.interacciones.framework.util.PrinterMargins.Margin;

/**
 * Clase con métodos para formatear el output de archivos Pdf.
 *
 *
 * @author Rodrigo Benítez
 * @since 1.0
 * @version 1.0 Sep 5, 2014
 *
 */

public class PdfFormat {

	/**
	 * Código extraído de
	 * http://stackoverflow.com/questions/6155921/rotate-single-page-of-document
	 *
	 * @throws IOException
	 * @throws DocumentException
	 */
	public static ByteArrayOutputStream rotateLandscape(ByteArrayInputStream file)
			throws IOException, DocumentException {

		PdfReader reader = new PdfReader(file);

		for (int i = 1; i <= reader.getNumberOfPages(); i++) {
			PdfDictionary pageDict = reader.getPageN(i);
			// pageDict.put(PdfName.ROTATE, new PdfNumber("45"));
		}

		ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
		PdfStamper stamper = new PdfStamper(reader, baos2);
		stamper.close();
		reader.close();
		return baos2;
	}

	/**
	 * Configura los margenes de un PDF
	 * 
	 * @param design
	 * @param margin
	 */
	public static void setMargins(JasperDesign design, Margin margin) {
		if (margin == null)
			return;

		if (margin.getX().isPresent()) {
			int real = ReportUtil.pxToCm(margin.getX().get());
			int oldLeftMargin = design.getLeftMargin();
			design.setLeftMargin(real);
			design.setColumnWidth(design.getColumnWidth() + oldLeftMargin - real);
		}

		if (margin.getY().isPresent()) {
			int real = ReportUtil.pxToCm(margin.getY().get());
			design.setTopMargin(real);
		}
	}
}
