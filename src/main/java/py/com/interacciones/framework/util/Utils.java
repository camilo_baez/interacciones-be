/*
 * @SortInterpreter.java 1.0 Sep 5, 2014
 * Sistema de Gestion de Procesos
 * 
 */
package py.com.interacciones.framework.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import py.com.interacciones.interacciones.domain.BaseEntity;

import javax.inject.Inject;
import javax.ws.rs.core.MultivaluedMap;

/**
 * 
 * @author sortiz
 * @since 1.0
 * @version 1.0 Sep 5, 2014
 * 
 */
public class Utils {

	public static final String ORDER_FORMAT_ASC = "asc";
	public static final String ORDER_FORMAT_DES = "des";
	public static final String ORDER_PATTERN = "(.*)\\.(.*)";

	@Inject
	Config config;
	
	/**
	 * Retorna un mapa de ordenes dada una lista serializada.
	 * 
	 * <p>
	 * Dado:
	 * 
	 * <pre>
	 * 		"nombre.asc"
	 * 		"apellido.des"
	 * 		"documento.descripcion.des"
	 * </pre>
	 * 
	 * Retorna:
	 * 
	 * <pre>
	 * 		"nombre" : true
	 * 		"apellido" : true
	 * 		"documento.descripcion" : false
	 * </pre>
	 * 
	 * @param sorts
	 * @return
	 */
	public static List<SortInfo> deserializeSort(List<String> sorts) {

		if (sorts == null || sorts.isEmpty())
			return new ArrayList<>();

		Pattern p = Pattern.compile(ORDER_PATTERN);
		ArrayList<SortInfo> toRet = new ArrayList<>();
		for (String s : sorts) {
			if (StringUtils.isEmpty(s))
				continue;
			Matcher matcher = p.matcher(s);
			if (!matcher.matches()) {
				throw new RuntimeException("Malformated order " + s);
			}

			String field = matcher.group(1);
			String order = matcher.group(2);

			toRet.add(new SortInfo(field, ORDER_FORMAT_ASC.equals(order)));

		}
		return toRet;
	}

	public static Map<String, String> joinAndCheckLists(List<String> properties, List<String> values,
			Class<?> entityClass) {

		if (properties == null || properties.isEmpty() || values == null || values.isEmpty())
			return Collections.emptyMap();

		if (properties.size() != values.size())
			throw new RuntimeException("Diferentes ordenes de propiedades y valores");

		Map<String, String> toRet = new HashMap<>();
		for (int i = 0; i < properties.size(); i++) {
			toRet.put(properties.get(i), values.get(i));
		}
		return toRet;
	}

	/**
	 * Retorna un {@link Date} que representa el último dia del mes y anho.
	 * 
	 * @param anho
	 *            anho del cual se desea su primer dia
	 * @param mes
	 *            mes del anho del cual se desea su primer dia
	 * @return {@link Date} representando el último dia, nunca <code>null</code>
	 */
	public static Date getDateFin(int anho, int mes) {
		Calendar fin = Calendar.getInstance();
		fin.clear();
		fin.set(anho, mes, 1);
		fin.add(Calendar.MONTH, 1);
		fin.getTime();
		fin.add(Calendar.DATE, -1);
		return fin.getTime();
	}

	/**
	 * Retorna un {@link Date} que representa el primer dia del mes y anho.
	 * 
	 * Notar que el mes que se pasa aquí empieza en 0
	 * 
	 * @param anho
	 *            anho del cual se desea su primer dia
	 * @param mes
	 *            mes del anho del cual se desea su primer dia
	 * @return {@link Date} representando el primer dia, nunca <code>null</code>
	 */
	public static Date getDateInicio(int anho, int mes) {
		Calendar inicio = Calendar.getInstance();
		inicio.clear();
		inicio.set(anho, mes, 1);
		return inicio.getTime();
	}
	public static Date getNextMonth(int anho, int mes) {
		Calendar inicio = Calendar.getInstance();
		inicio.clear();
		inicio.set(anho, mes, 1);
		inicio.add(Calendar.MONTH, 1);
		Date dInicio = inicio.getTime();
		return dInicio;
	}

	public static <T extends BaseEntity> T getEntidad(MultipartFormDataInput input, Class<T> type) throws IllegalArgumentException{
		return getEntidad(input, null, type);
	}

	public static <T extends BaseEntity> T getEntidad(MultipartFormDataInput input, String nombreEntidad, Class<T> type) throws IllegalArgumentException{
		Map<String, List<InputPart>> uploadForm = input.getFormDataMap();

		List<InputPart> procesoInputPart = uploadForm.get(nombreEntidad != null ? nombreEntidad : "entidad");
		//Se borra el proceso del multiparte para poder iterar sobre los archivos
		uploadForm.remove("entidad");
		T entidad = null;
		if (procesoInputPart != null) {
			Scanner s = null;
			try {
				InputPart inputPart = procesoInputPart.get(0);
				InputStream istream = inputPart.getBody(InputStream.class, null);
				s = new Scanner(istream).useDelimiter("\\A");
				String procesoString = s.hasNext() ? s.next() : "";
				if (procesoString.equals(""))
					throw new IllegalArgumentException("No existen fiscalizaciones.");
				ObjectMapper mapper = new ObjectMapper();
				entidad = mapper.readValue(procesoString, type);
				return entidad;
			} catch (IOException e) {
				e.printStackTrace();
				throw new IllegalArgumentException("Ocurrió un error al guardar el proceso");
			} finally {
				if (s != null)
					s.close();
			}
		}
		return null;
	}

	public static List<ImmutablePair<String, String>> getDocumentos(MultipartFormDataInput input, String fileServer) throws IllegalArgumentException{
		Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
		List<InputPart> inputParts;
		InputPart inputPart;
		MultivaluedMap<String, String> header;
		InputStream inputStream;
		//Listado de documentos a asignar al proceso
		List<ImmutablePair<String, String>> archivos = new ArrayList<>();
		//Cada archivo genera una instancia Documento
		String fileName = "";

		try {
			//Se asume que vienen solo archivos
			for (Map.Entry<String, List<InputPart>> entry : uploadForm.entrySet())
			{
				if(entry.getKey().contains("files")){
					inputParts = entry.getValue();
					inputPart = inputParts.get(0);

					// Documento a retornar
					fileName = "";

					header = inputPart.getHeaders();
					fileName = getFileName(header);

					// Convert the uploaded file to inputstream
					inputStream = inputPart.getBody(InputStream.class, null);
					byte[] bytes = IOUtils.toByteArray(inputStream);

					// Constructs upload file path
					LocalDateTime now = LocalDateTime.now();
					String url = fileServer + String.format("%04d", now.getYear())
							+ String.format("%02d", now.getMonthValue()) + String.format("%02d", now.getDayOfMonth()) + "_"
							+ String.format("%02d", now.getHour()) + String.format("%02d", now.getMinute())
							+ String.format("%02d", now.getSecond()) + "_" + fileName;

					writeFile(bytes, url);
					archivos.add(new ImmutablePair<>(fileName, url));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			//Se eliminan todos los documentos del Sistema de Archivos
			//Se podria ver la forma de que guarde los demas archivos que no tienen problemas
			borrarArchivos(archivos);
			throw new IllegalArgumentException("Ocurrió un error al guardar el documento: "+ fileName);
		}
		return archivos;
	}

	public static HashMap<String, InputStream> getData(MultipartFormDataInput input) throws IllegalArgumentException{
		Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
		List<InputPart> inputParts;
		InputPart inputPart;
		MultivaluedMap<String, String> header;
		InputStream inputStream = null;
		String fileName = "";
		try {
			//Se asume que vienen solo archivos
			for (Map.Entry<String, List<InputPart>> entry : uploadForm.entrySet())
			{
				if(entry.getKey().contains("files")){
					inputParts = entry.getValue();
					inputPart = inputParts.get(0);

					header = inputPart.getHeaders();
					fileName = getFileName(header);

					// Convert the uploaded file to inputstream
					inputStream = inputPart.getBody(InputStream.class, null);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new IllegalArgumentException("Ocurrió un error al guardar el documento: "+ fileName);
		}
		HashMap<String, InputStream> map = new HashMap<>();
		map.put(fileName, inputStream);
		return map;
	}

	public static String getFileName(MultivaluedMap<String, String> header) {
		String[] contentDisposition = header.getFirst("Content-Disposition").split(";");
		for (String filename : contentDisposition) {
			if ((filename.trim().startsWith("filename"))) {
				String[] name = filename.split("=");
				String finalFileName = name[1].trim().replaceAll("\"", "");
				return finalFileName;
			}
		}
		return "unknown";
	}

	public static void writeFile(byte[] content, String filename) throws IOException {
		File file = new File(filename);
		if (!file.exists()) {
			file.createNewFile();
		}
		FileOutputStream fop = new FileOutputStream(file);
		fop.write(content);
		fop.flush();
		fop.close();
	}
	public static void borrarArchivo(String filename) {
		try{
			File file = new File(filename);
			file.delete();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public static void borrarArchivos(List<ImmutablePair<String, String>> archivos){
		for(Pair<String, String> archivo : archivos){
			borrarArchivo(archivo.getRight());//url
		}
	}

	public static Workbook getWorkbook(InputStream inputStream, String excelFilePath)
			throws IOException {
		Workbook workbook = null;

		if (excelFilePath.endsWith("xlsx")) {
			workbook = new XSSFWorkbook(inputStream);
		} else if (excelFilePath.endsWith("xls")) {
			workbook = new HSSFWorkbook(inputStream);
		} else {
			throw new IllegalArgumentException("No es un archivo Excel");
		}

		return workbook;
	}

	public static Object getCellValue(Cell cell) {
		try {
			switch (cell.getCellType()) {
				case Cell.CELL_TYPE_STRING:
					return cell.getStringCellValue();

				case Cell.CELL_TYPE_BOOLEAN:
					return cell.getBooleanCellValue();

				case Cell.CELL_TYPE_NUMERIC:
					return cell.getNumericCellValue();
				default:
					return cell.getDateCellValue();
			}
		}catch (Exception ex){
			throw ex;
		}
	}
}
