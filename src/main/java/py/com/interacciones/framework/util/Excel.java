package py.com.interacciones.framework.util;

import org.apache.commons.lang3.Validate;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import py.com.interacciones.interacciones.domain.SalidaExcel;
import py.com.interacciones.interacciones.logic.MedicamentoLogic;
import py.com.interacciones.interacciones.logic.PacienteLogic;

import javax.inject.Inject;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by camilo on 08/08/17.
 */
public class Excel {
    private static final String FILE_NAME = "/opt/excel.xlsx";

    public static final String SALIDA_ID = "ID_SALIDA_STOCK";
    public static final String PACIENTE_ID = "ID_PACIENTE";
    public static final String PACIENTE_NOMBRE = "PACIENTE";
    public static final String MEDICAMENTO_ID = "DEP_ART_ID_ARTICULO";
    public static final String MEDICAMENTO_NOMBRE = "ARTICULO";
    public static final String FECHA = "FECHA";
    public static final String CANTIDAD = "CANTIDAD";
    public static final int filaEncabezado = 0;
    int salidaId = -1;
    int pacienteId = -1;
    int pacienteNombre = -1;
    int medicamentoId = -1;
    int medicamentoNombre = -1;
    int fecha = -1;
    int cantidad = -1;

    @Inject
    PacienteLogic pacienteLogic;
    @Inject
    MedicamentoLogic medicamentoLogic;

    public List<SalidaExcel> obtenerDelExcel(String excelFilePath, InputStream inputStream) throws Exception {
        List<SalidaExcel> salidas = new ArrayList<>();
        Workbook workbook = Utils.getWorkbook(inputStream, excelFilePath);
        Sheet firstSheet = workbook.getSheetAt(0);

        Iterator<Row> iterator = firstSheet.iterator();

        verificarEncabezadoSalida(iterator);

        //Los datos estan justamente debajo del encabezado
        while (iterator.hasNext()) {
            Row nextRow = iterator.next();
            SalidaExcel salidaExcel = new SalidaExcel();

            validarDatosObligatorios(nextRow);

            try{
                salidaExcel.setFila(nextRow.getRowNum() + 1);

                salidaExcel.setCodigo(nextRow.getCell(salidaId) == null ? null : ((Double) Utils.getCellValue(nextRow.getCell(salidaId))).longValue());
                salidaExcel.setPaciente(nextRow.getCell(pacienteId) == null ? null : ((Double) Utils.getCellValue(nextRow.getCell(pacienteId))).longValue());
                salidaExcel.setPacienteNombre(nextRow.getCell(pacienteNombre) == null ? null : (String) Utils.getCellValue(nextRow.getCell(pacienteNombre)));
                salidaExcel.setMedicamento(nextRow.getCell(medicamentoId) == null ? null : ((Double) Utils.getCellValue(nextRow.getCell(medicamentoId))).longValue());
                salidaExcel.setMedicamentoNombre(nextRow.getCell(medicamentoNombre) == null ? null : (String) Utils.getCellValue(nextRow.getCell(medicamentoNombre)));
                salidaExcel.setFecha(nextRow.getCell(fecha) == null ? null : nextRow.getCell(fecha).getDateCellValue());
                Object cantidadValue = nextRow.getCell(cantidad) == null ? null :Utils.getCellValue(nextRow.getCell(cantidad));
                if(cantidadValue == null){
                    salidaExcel.setCantidad(null);
                }else if(cantidadValue instanceof String){
                    //cantidadValue = ((String) cantidadValue).replace(".", ",");
                    Double canDouble = Double.parseDouble((String)cantidadValue);
                    Double cantidadDouble = new Double((String)cantidadValue);

                    salidaExcel.setCantidad(cantidadDouble.intValue());
                }else{
                    salidaExcel.setCantidad(((Double) cantidadValue).intValue());
                }

                salidas.add(salidaExcel);
            }catch (Exception ex){
                throw getException(ex, salidaExcel, nextRow);
            }
        }

        workbook.close();
        inputStream.close();

        return salidas;
    }

    public void validarDatosObligatorios(Row nextRow){
        Validate.notNull(nextRow.getCell(salidaId), "El campo " + SALIDA_ID +
                " no puede estar vacío. Fila: " + (nextRow.getRowNum() + 1));
        Validate.notNull(nextRow.getCell(pacienteId), "El campo " + PACIENTE_ID +
                " no puede estar vacío. Fila: " + (nextRow.getRowNum() + 1));
        Validate.notNull(nextRow.getCell(medicamentoId), "El campo " + MEDICAMENTO_ID +
                " no puede estar vacío. Fila: " + (nextRow.getRowNum() + 1));
        Validate.notNull(nextRow.getCell(fecha), "El campo " + FECHA +
                " no puede estar vacío. Fila: " + (nextRow.getRowNum() + 1));
        Validate.notNull(nextRow.getCell(cantidad), "El campo " + CANTIDAD +
                " no puede estar vacío. Fila: " + (nextRow.getRowNum() + 1));
    }
    public Exception getException(Exception ex, SalidaExcel salidaExcel, Row nextRow){
        String fila = nextRow.getRowNum() + 1 +"";
        String columna = "";
        String valor = "";

        if(salidaExcel.getCodigo() == null){
            columna = SALIDA_ID;
            valor = nextRow.getCell(salidaId).getStringCellValue();
        }else if (salidaExcel.getPaciente() == null){
            columna = PACIENTE_ID;
            Object ob = nextRow.getCell(pacienteId);
            valor = nextRow.getCell(pacienteId).getStringCellValue();
        }else if (salidaExcel.getPacienteNombre() == null){
            columna = PACIENTE_NOMBRE;
            valor = nextRow.getCell(pacienteNombre).getStringCellValue();
        }else if (salidaExcel.getMedicamento() == null){
            columna = MEDICAMENTO_ID;
            valor = nextRow.getCell(medicamentoId).getStringCellValue();
        }else if (salidaExcel.getMedicamentoNombre() == null){
            columna = MEDICAMENTO_NOMBRE;
            valor = nextRow.getCell(medicamentoNombre).getStringCellValue();
        }else if (salidaExcel.getFecha() == null){
            columna = FECHA;
            valor = nextRow.getCell(fecha).getStringCellValue();
        }else if (salidaExcel.getCantidad() == null){
            columna = CANTIDAD;
            valor = nextRow.getCell(cantidad).getStringCellValue();
        }

        if(ex instanceof ClassCastException){
            if(ex.getMessage().equals("java.lang.String cannot be cast to java.lang.Double")){
                return new IllegalArgumentException("El valor '"+ valor + "' debe ser numérico (Ej: 12,5)" +
                        "<br/>Fila: " + fila+
                        "<br/>Columna: " + columna);
            }
        }else if(ex instanceof IllegalStateException){
            if(ex.getMessage().equals("Cannot get a NUMERIC value from a STRING cell")){
                return new IllegalArgumentException("El valor '"+ valor + "' debe ser una fecha válida (Ej: 20/01/2017" +
                        "<br/>Fila: " + fila+
                        "<br/>Columna: " + columna);
            }
        }
        return new IllegalArgumentException("Verificar el valor '"+ valor + "'" +
                "<br/>Fila: " + fila+
                "<br/>Columna: " + columna);
    }

    private void verificarEncabezadoSalida(Iterator<Row> iterator) {
        Row nextRow = iterator.next();

        //Omite las filas anteriores al encabezado
        while (iterator.hasNext() && nextRow.getRowNum() < filaEncabezado) {
            nextRow = iterator.next();
        }

        Iterator<Cell> cellIterator = nextRow.cellIterator();
        boolean configurado = false;
        while (cellIterator.hasNext()) {
            Cell nextCell = cellIterator.next();
            setPositionsSalida(nextCell);
            //Si ya configuró todas las columnas, terminar
            if (-1 != salidaId
                    && -1 != pacienteId
                    && -1 != pacienteNombre
                    && -1 != medicamentoId
                    && -1 != medicamentoNombre
                    && -1 != fecha
                    && -1 != cantidad) {
                configurado = true;
                break;
            }
        }

        if(!configurado){
            if(salidaId == -1){
                throw new IllegalArgumentException("Verificar encabezado: " + SALIDA_ID);
            }
            if(pacienteId == -1){
                throw new IllegalArgumentException("Verificar encabezado: " + PACIENTE_ID);
            }
            if(pacienteNombre == -1){
                throw new IllegalArgumentException("Verificar encabezado: " + PACIENTE_NOMBRE);
            }
            if(medicamentoId == -1){
                throw new IllegalArgumentException("Verificar encabezado: " + MEDICAMENTO_ID);
            }
            if(medicamentoNombre == -1){
                throw new IllegalArgumentException("Verificar encabezado: " + MEDICAMENTO_NOMBRE);
            }
            if(fecha == -1){
                throw new IllegalArgumentException("Verificar encabezado: " + FECHA);
            }
            if(cantidad == -1){
                throw new IllegalArgumentException("Verificar encabezado: " + CANTIDAD);
            }
        }
    }

    private void setPositionsSalida(Cell nextCell) {
        switch (nextCell.getStringCellValue()) {
            case SALIDA_ID:
                salidaId = nextCell.getColumnIndex();
                break;
            case PACIENTE_ID:
                pacienteId = nextCell.getColumnIndex();
                break;
            case PACIENTE_NOMBRE:
                pacienteNombre = nextCell.getColumnIndex();
                break;
            case MEDICAMENTO_ID:
                medicamentoId = nextCell.getColumnIndex();
                break;
            case MEDICAMENTO_NOMBRE:
                medicamentoNombre = nextCell.getColumnIndex();
                break;
            case FECHA:
                fecha = nextCell.getColumnIndex();
                break;
            case CANTIDAD:
                cantidad = nextCell.getColumnIndex();
                break;
        }

    }
}
