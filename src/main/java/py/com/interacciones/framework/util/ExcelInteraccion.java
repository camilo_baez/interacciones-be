package py.com.interacciones.framework.util;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import py.com.interacciones.interacciones.domain.InteraccionExcel;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by camilo on 08/08/17.
 */
public class ExcelInteraccion {
    public static final String ACTIVO_A = "ACTIVO_A";
    public static final String ACTIVO_B = "ACTIVO_B";
    public static final String EFECTO = "EFECTO";
    public static final String SEVERIDAD_NOMBRE = "SEVERIDAD";
    public static final String SEVERIDAD_ID = "ID_SEVERIDAD";

    public static final int filaEncabezado = 0;
    int activoA = -1;
    int activoB = -1;
    int efecto = -1;
    int severidad = -1;
    int severidadId = -1;

    public List<InteraccionExcel> obtenerDelExcel(String excelFilePath, InputStream inputStream) throws IOException {
        List<InteraccionExcel> interacciones = new ArrayList<>();
        Workbook workbook = Utils.getWorkbook(inputStream, excelFilePath);
        Sheet firstSheet = workbook.getSheetAt(0);

        Iterator<Row> iterator = firstSheet.iterator();

        verificarEncabezado(iterator);

        //Los datos estan justamente debajo del encabezado
        while (iterator.hasNext()) {
            Row nextRow = iterator.next();
            InteraccionExcel interaccionExcel = new InteraccionExcel();
            try {
                interaccionExcel.setActivoA((String) Utils.getCellValue(nextRow.getCell(activoA)));
                interaccionExcel.setActivoB((String) Utils.getCellValue(nextRow.getCell(activoB)));
                interaccionExcel.setSeveridad((String) Utils.getCellValue(nextRow.getCell(severidad)));
                interaccionExcel.setEfecto((String) Utils.getCellValue(nextRow.getCell(efecto)));
            }catch(NullPointerException ex){
            }
            interacciones.add(interaccionExcel);
        }

        workbook.close();
        inputStream.close();

        return interacciones;
    }

    private void verificarEncabezado(Iterator<Row> iterator) {
        Row nextRow = iterator.next();

        //Omite las filas anteriores al encabezado
        while (iterator.hasNext() && nextRow.getRowNum() < filaEncabezado) {
            nextRow = iterator.next();
        }

        Iterator<Cell> cellIterator = nextRow.cellIterator();
        boolean configurado = false;
        while (cellIterator.hasNext()) {
            Cell nextCell = cellIterator.next();
            setPositions(nextCell);
            //Si ya configuró todas las columnas, terminar
            if (-1 != activoA
                    && -1 != activoB
                    && -1 != severidad
                    && -1 != efecto) {
                configurado = true;
                break;
            }
        }

        if(!configurado){
            if(activoA == -1){
                throw new IllegalArgumentException("Verificar encabezado: " + ACTIVO_A);
            }
            if(activoB == -1){
                throw new IllegalArgumentException("Verificar encabezado: " + ACTIVO_B);
            }
        }
    }

    private void setPositions(Cell nextCell) {
        switch (nextCell.getStringCellValue()) {
            case ACTIVO_A:
                activoA = nextCell.getColumnIndex();
                break;
            case ACTIVO_B:
                activoB = nextCell.getColumnIndex();
                break;
            case EFECTO:
                efecto = nextCell.getColumnIndex();
                break;
            case SEVERIDAD_ID:
                severidadId = nextCell.getColumnIndex();
                break;
            case SEVERIDAD_NOMBRE:
                severidad = nextCell.getColumnIndex();
                break;
        }

    }
}
