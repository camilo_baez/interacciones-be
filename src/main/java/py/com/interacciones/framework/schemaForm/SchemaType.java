package py.com.interacciones.framework.schemaForm;

import java.lang.annotation.*;

/**
 * Created by sortiz on 7/7/16.
 */
@Target({ ElementType.FIELD })
@Retention(value = RetentionPolicy.RUNTIME)
public @interface SchemaType {

	/**
	 * Indica el valor de la propiedad type, si no se especifica se usa el de la clase
	 **/
	String value();
}
