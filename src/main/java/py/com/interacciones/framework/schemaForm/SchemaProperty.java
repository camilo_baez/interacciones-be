package py.com.interacciones.framework.schemaForm;

import java.lang.annotation.*;

/**
 * Created by sortiz on 7/7/16.
 */
@Target({ ElementType.FIELD })
@Retention(value = RetentionPolicy.RUNTIME)
@Repeatable(SchemaProperties.class)
public @interface SchemaProperty {

	/**
	 * Indica el nombre de la propiedad.
	 **/
	String key();
	/**
	 * Indica el valor de la propiedad definida en key.
	 **/
	String value();
}
