package py.com.interacciones.framework.schemaForm;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by sortiz on 7/7/16.
 */
@Target({ ElementType.FIELD })
@Retention(value = RetentionPolicy.RUNTIME)
public @interface SchemaNullable {

	/**
	 * Indica que se ignore el @NotNull
	 **/

}
