package py.com.interacciones.framework.schemaForm;

import java.lang.annotation.*;

/**
 * Created by sortiz on 7/7/16.
 */
@Target({ ElementType.FIELD })
@Retention(value = RetentionPolicy.RUNTIME)
public @interface SchemaProperties {

	/**
	 * Indica el valor de las propiedades definidas con el annotation SchemaProperty.
	 **/
	SchemaProperty[] value();
}
