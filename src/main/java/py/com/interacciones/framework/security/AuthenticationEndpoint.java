package py.com.interacciones.framework.security;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.tuple.Pair;

import io.swagger.annotations.Api;
import py.com.interacciones.interacciones.domain.Usuario;

@Api("login")
@Path("/authentication")
public class AuthenticationEndpoint {

	@Inject
	SessionTokenInfo info;

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes("application/x-www-form-urlencoded")
    public Response authenticateUser(@FormParam("username") String username, @FormParam("password") String password) {

		try {

			Map<String, Object> toRet = new HashMap<>();
			Pair<Usuario, String> pair = info.authenticate(username, password);
			toRet.put("user", pair.getLeft());
			toRet.put("token", pair.getRight());
			// Return the token on the response
			return Response.ok().entity(toRet).build();

		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
	}

	@GET
	@Path("/logout")
	public Response logout(@HeaderParam(HttpHeaders.AUTHORIZATION) String token) {

		try {

			info.logout(token);
			return Response.ok().build();
		} catch (Exception e) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}
	}

	@GET
	@Path("/isLogged")
	public Response isLogged() {
		return Response.ok().build();
	}

}