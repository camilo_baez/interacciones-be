package py.com.interacciones.framework.security;

import java.io.Serializable;
import java.security.GeneralSecurityException;
import java.util.Base64;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import org.apache.commons.lang3.tuple.ImmutablePair;

import py.com.interacciones.framework.util.Config;
import py.com.interacciones.interacciones.domain.Usuario;
import py.com.interacciones.interacciones.logic.UserOrPasswordMismatchException;
import py.com.interacciones.interacciones.logic.UsuarioLogic;
import redis.clients.jedis.Jedis;

@RequestScoped
public class SessionTokenInfo implements Serializable {

	private static final long serialVersionUID = -6825391877753604019L;

	@Inject
	UsuarioLogic logic;

	@Inject
	Jedis jedis;

	@Inject
	Config config;

	public ImmutablePair<Usuario, String> authenticate(String username, String password)
			throws UserOrPasswordMismatchException, GeneralSecurityException {
		Usuario user = logic.login(username, password);
		String token = issueToken();

		jedis.set(token, user.getId() + "");
		jedis.expire(token, config.getSessionTimeout());

		return new ImmutablePair<>(user, token);
	}

	private String issueToken() throws GeneralSecurityException {
		// Issue a token (can be a random String persisted to a database or a
		// JWT token)
		// The issued token must be associated to a user
		// Return the issued token
		KeyGenerator keyGen = KeyGenerator.getInstance("AES");
		keyGen.init(256); // for example
		SecretKey secretKey = keyGen.generateKey();
		String encodedKey = Base64.getEncoder().encodeToString(secretKey.getEncoded());
		return encodedKey;
	}

	public void logout(String token) {
		jedis.del(token);
	}

	/**
	 * Invalida todas las sesiones de un usuario.
	 *
	 * @param u
	 */
	public void logout(Usuario u) {
		jedis.keys(u.getId() + "").stream().forEach(this::logout);
	}

	/**
	 * Revisa si existe el token
	 *
	 * @param token
	 *            a verificar
	 * @throws NotExistentTokenException
	 *             si el token no exite.
	 * @return nombre del usuario cuyo token se valido
	 */
	public Usuario validate(String token) {
		String username = jedis.get(token);
		jedis.expire(token, config.getSessionTimeout());
		if (username == null)
			throw new NotExistentTokenException();
		else
			return logic.getById(Long.valueOf(username)).get();
	}
}
