package py.com.interacciones.framework.security;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URLDecoder;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;

import py.com.interacciones.interacciones.domain.Usuario;
import py.com.interacciones.interacciones.logic.UsuarioLogic;

@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter {

	@Inject
	SessionTokenInfo tokenInfo;

	@Inject
	UserSessionDataHolder dataHolder;

	@Inject
	UsuarioLogic logic;

	@Context
	ResourceInfo resourceInfo;

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {

		UriInfo info = requestContext.getUriInfo();

		if (info.getPath().equals("/authentication") || info.getPath().equals("/swagger.json")) {
			return;
		}
		// Get the HTTP Authorization header from the request

		String authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
		String token = null;

		if (authorizationHeader != null) {
			// Check if the HTTP Authorization header is present and formatted
			// correctly
			if (authorizationHeader.startsWith("Bearer ")) {
				token = authorizationHeader.substring("Bearer".length()).trim();
			} else {
				token = authorizationHeader.trim();
			}
		} else {
			Cookie cook = requestContext.getCookies().get(HttpHeaders.AUTHORIZATION);
			if (cook != null) {
				token = URLDecoder.decode(cook.getValue(), "UTF-8");
			} else {
				token = info.getQueryParameters().getFirst("api_key");
			}
		}

		try {

			// Validate the token
			Usuario user = tokenInfo.validate(token);
			// Guardar el usuario par ainformación de auditoria
			dataHolder.setUser(user);

			// Obtenemos el metodo y vemos si puede usar ?
			Method theMethod = resourceInfo.getResourceMethod();

			if (!userHasPermission(user, theMethod)) {
				requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED)
						.entity("{ \"notAllowed\" : true, \"message\" : \"No tiene permiso para acceder a esta sección\"}")
						.build());
			}

		} catch (Exception e) {
			requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
		}
	}

	private boolean userHasPermission(Usuario user, Method theMethod) {
		if (!theMethod.isAnnotationPresent(Require.class)) {
			return true;
		}

		return logic.hasPermission(user, theMethod.getAnnotation(Require.class).value());
	}

}
