package py.com.interacciones.framework.security;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;

import lombok.AccessLevel;
import lombok.Setter;
import py.com.interacciones.framework.util.Config;
import py.com.interacciones.interacciones.domain.Usuario;
import py.com.interacciones.interacciones.logic.UsuarioLogic;

@SessionScoped
public class UserSessionDataHolder implements Serializable {

	private static final long serialVersionUID = 9000522619431456058L;

	@Setter(AccessLevel.PROTECTED)
	Usuario user;

	@Inject
	UsuarioLogic usuarioLogic;

	@Inject
	Config config;

	public Usuario getUser() {
		
		if (user == null && config.isDevel()) {
			Usuario dePruebas = new Usuario();
			dePruebas.setId(1000L);
			dePruebas.setNombre("ADMIN");
			dePruebas.setUsername("ADMIN");
			user = dePruebas;
		}
		return user;
	}

}
