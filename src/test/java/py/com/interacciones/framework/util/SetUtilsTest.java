package py.com.interacciones.framework.util;

import static org.junit.Assert.*;

import org.junit.Test;

public class SetUtilsTest {

	@Test
	public void digitos_conocidos_test() {

		assertEquals(1, SetUtils.getDigitoVerificador("CD351-3-3110-16"));
		assertEquals(9, SetUtils.getDigitoVerificador("4787587"));
		assertEquals(1, SetUtils.getDigitoVerificador("80089716"));
		assertEquals(0, SetUtils.getDigitoVerificador("3463897"));
		assertEquals(4, SetUtils.getDigitoVerificador("4491884"));
		assertEquals(3, SetUtils.getDigitoVerificador("80000127"));

	}

	@Test
	public void testName() throws Exception {

		System.out.println(String.format("%1$02d", 1));
	}
}
