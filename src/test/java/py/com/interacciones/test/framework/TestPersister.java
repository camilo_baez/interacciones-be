package py.com.interacciones.test.framework;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class TestPersister {

	@PersistenceContext
	EntityManager em;

	public void persist(Map<Class<?>, List<Object>> list) {
		for (Entry<Class<?>, List<Object>> e : list.entrySet()) {
			for (Object o : e.getValue()) {
				System.out.println("Agregando: " + o.toString());
				em.merge(o);
			}
		}
	}

	public void remove(Map<Class<?>, List<Object>> list) {
		for (Entry<Class<?>, List<Object>> e : list.entrySet()) {
			for (Object o : e.getValue()) {
				em.remove(o);
			}
		}
	}

	public void load(String sql) {
		
		System.out.println("Metidas " + em.createNativeQuery(sql).executeUpdate() + " filas");
	}
}
