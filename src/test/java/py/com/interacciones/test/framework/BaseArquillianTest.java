package py.com.interacciones.test.framework;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Filters;
import org.jboss.shrinkwrap.api.GenericArchive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.importer.ExplodedImporter;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.jboss.shrinkwrap.resolver.api.maven.ScopeType;
import org.junit.runner.RunWith;

import py.com.interacciones.framework.util.Utils;
import py.com.interacciones.interacciones.IncludeMe;
import py.com.interacciones.interacciones.config.DBTestConfig;
import py.com.interacciones.interacciones.config.EntityManagerProvider;
import py.com.interacciones.interacciones.config.RestApplication;

@RunWith(Arquillian.class)
public abstract class BaseArquillianTest {

	@Deployment
	public static WebArchive createDeployment() throws Exception {

		//@formatter:off
		WebArchive toRet = ShrinkWrap.create(WebArchive.class)
				.addPackages(true, IncludeMe.class.getPackage())
				.addPackages(true, Utils.class.getPackage())
				.addClasses(DBTestConfig.class, EntityManagerProvider.class)
				
				// Mientras no haya servicios hay que agregar a mano otra
				.addClass(RestApplication.class)
				.addClass(RestPopulator.class)
				.addClass(TestPersister.class)
				
				.addAsResource("META-INF/resources/core/")
			
				.addAsResource("META-INF/persistence.xml")

				.addAsWebInfResource(new File("src/main/webapp/WEB-INF/web.xml"))
				.addAsWebInfResource(new File("src/test/resources/META-INF/beans.xml"))
				
				.addAsLibraries(Maven.resolver().loadPomFromFile("pom.xml")
						.importDependencies(ScopeType.COMPILE)
						.resolve()
						.withTransitivity()

						.asFile());
		
		Files.walk(Paths.get("src/test/resources/cucumber"))
			.map(e -> e.toString())
			.filter(e -> e.endsWith("feature"))
			.map(e -> e.substring(e.indexOf("cucumber")))
			.forEach(e -> toRet.addAsResource(e));

		
		Files.walk(Paths.get("src/test/resources/datasets"))
			.map(e -> e.toString())
			.filter(e -> e.endsWith("json") || e.endsWith("sql"))
			.map(e -> e.substring(e.indexOf("datasets")))
			.forEach(e -> toRet.addAsResource(e));

		toRet.merge(ShrinkWrap.create(GenericArchive.class).as(ExplodedImporter.class)  
			.importDirectory("src/main/webapp/").as(GenericArchive.class),  
			"/", Filters.include(".*\\.xhtml$"));

		toRet.merge(ShrinkWrap.create(GenericArchive.class).as(ExplodedImporter.class)  
			.importDirectory("src/main/webapp/").as(GenericArchive.class),  
			"/", Filters.include(".*\\.css$"));

		toRet.merge(ShrinkWrap.create(GenericArchive.class).as(ExplodedImporter.class)  
			.importDirectory("src/main/webapp/").as(GenericArchive.class),  
			"/", Filters.include(".*\\.png$"));
		//@formatter:on

		System.out.println(toRet.toString(true));

		return toRet;
	}
}
