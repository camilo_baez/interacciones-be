package py.com.interacciones.test.framework;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import org.apache.commons.io.IOUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Path("persistence")
public class RestPopulator {

	@Inject
	TestPersister persister;

	@GET
	@Path("load/{dataset}")
	public void loadData(@PathParam("dataset") String dataset) throws Exception {

		if (dataset.endsWith("sql")) {
			persister.load(readFile(dataset));
		} else {
			persister.persist(loadDataset(dataset));
		}

	}

	private String readFile(String dataset) throws Exception {

		try (InputStream is = getClass().getClassLoader().getResourceAsStream("datasets/" + dataset)) {
			return IOUtils.toString(is);
		}
	}

	@GET
	@Path("remove/{dataset}")
	public void deleteData(@PathParam("dataset") String dataset) throws Exception {

		persister.remove(loadDataset(dataset));

	}

	private Map<Class<?>, List<Object>> loadDataset(String dataset) throws Exception {

		Map<Class<?>, List<Object>> data = new LinkedHashMap<>();

		ObjectMapper om = new ObjectMapper();

		InputStream is = getClass().getClassLoader().getResourceAsStream("datasets/" + dataset);

		ObjectNode jn = (ObjectNode) om.readTree(is);
		is.close();

		Iterator<Entry<String, JsonNode>> iter = jn.fields();
		while (iter.hasNext()) {
			Entry<String, JsonNode> entry = iter.next();

			Class<?> clazz = getEntity(entry.getKey());
			List<Object> list = new ArrayList<>();

			ArrayNode an = (ArrayNode) entry.getValue();

			for (JsonNode instance : an) {
				Object o = om.treeToValue(instance, clazz);
				list.add(o);
			}

			data.put(clazz, list);

		}

		return data;
	}

	private Class<?> getEntity(String name) throws Exception {

		if (name.contains(".")) {

			return Class.forName(name);

		}

		name = name.substring(0, 1).toUpperCase() + name.substring(1);

		return Class.forName("py.com.interacciones.interacciones.domain." + name);
	}
}
