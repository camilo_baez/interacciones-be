package py.com.interacciones.test.framework;

import java.net.HttpURLConnection;
import java.net.URL;

public class DBUtils {

	public static void createRemoteDataset(URL context, String dataset) {
		HttpURLConnection con = null;
		try {
			URL obj = new URL(context + "rest/persistence/load/" + dataset);

			con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("GET");
			con.setDoOutput(true);

			int responseCode = con.getResponseCode();
			if (responseCode != 200 && responseCode != 204) {
				throw new RuntimeException("Could not create remote dataset\nstatus:" + responseCode + "\nerror:"
						+ con.getResponseMessage());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				con.disconnect();
			}
		}
	}

	public static void deleteRemoteDataset(URL context, String dataset) {
		HttpURLConnection con = null;
		try {
			URL obj = new URL(context + "rest/persistence/remove/" + dataset);

			con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("GET");
			con.setDoOutput(true);

			int responseCode = con.getResponseCode();
			if (responseCode != 200 && responseCode != 204) {
				throw new RuntimeException("Could not create remote dataset\nstatus:" + responseCode + "\nerror:"
						+ con.getResponseMessage());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				con.disconnect();
			}
		}
	}
}
