package py.com.interacciones.interacciones.model.report;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import py.com.interacciones.framework.util.SetUtils;
import py.com.interacciones.framework.util.SetUtils.MontoConIva;
import py.com.interacciones.framework.util.SetUtils.RUC;

public class HechaukaGeneratorTest {

	@Test
	public void test_calcular_iva() throws Exception {

		MontoConIva mci = SetUtils.calcularImpuesto(100l, 10);

		assertEquals(91, mci.getMonto());
		assertEquals(9, mci.getIva());

		assertEquals(mci.getIva() + mci.getMonto(), 100l);
	}

	@Test
	public void test_calcular_iva_5() throws Exception {

		MontoConIva mci = SetUtils.calcularImpuesto(100l, 5);

		assertEquals(95, mci.getMonto());
		assertEquals(5, mci.getIva());
		assertEquals(mci.getIva() + mci.getMonto(), 100l);
	}

	@Test
	public void test_calcular_iva_10() throws Exception {

		MontoConIva mci = SetUtils.calcularImpuesto(5000l, 10);

		assertEquals(4545, mci.getMonto());
		assertEquals(455, mci.getIva());
		assertEquals(mci.getIva() + mci.getMonto(), 5000l);
	}

	@Test
	public void test_calcular_ruc_menor() throws Exception {

		RUC r = SetUtils.getRuc("MENOR", "MENOR");

		assertEquals("IMPORTES CONSOLIDADOS", r.getRazonSocial());
		assertEquals("44444401", r.getRuc());
		assertEquals("7", r.getDv());
	}

	@Test
	public void test_calcular_ruc_valido() throws Exception {

		RUC r = SetUtils.getRuc("4787587-9", "Arturo");

		assertEquals("Arturo", r.getRazonSocial());
		assertEquals("4787587", r.getRuc());
		assertEquals("9", r.getDv());
	}

	@Test
	public void test_calcular_ruc_invalido() throws Exception {

		RUC r = SetUtils.getRuc("4787587-", "Arturo");

		assertEquals("IMPORTES CONSOLIDADOS", r.getRazonSocial());
		assertEquals("44444401", r.getRuc());
		assertEquals("7", r.getDv());
	}

	@Test
	public void test_calcular_ruc_invalido_1() throws Exception {

		RUC r = SetUtils.getRuc("-1", "Arturo");

		assertEquals("IMPORTES CONSOLIDADOS", r.getRazonSocial());
		assertEquals("44444401", r.getRuc());
		assertEquals("7", r.getDv());
	}
}
