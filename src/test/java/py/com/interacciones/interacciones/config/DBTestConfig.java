package py.com.interacciones.interacciones.config;

import javax.annotation.sql.DataSourceDefinition;
import javax.ejb.Stateless;

@DataSourceDefinition(name = "java:/interacciones2", className = "org.h2.jdbcx.JdbcDataSource", url = "jdbc:h2:mem:test2")
@Stateless
public class DBTestConfig {

}
