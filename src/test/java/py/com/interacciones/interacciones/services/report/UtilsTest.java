package py.com.interacciones.interacciones.services.report;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;
import java.util.Date;

import org.junit.Test;

public class UtilsTest {

	@Test
	public void get_first_day_of_period_test() throws Exception {

		Date ld = Utils.getFirstDayOfPeriod("201601");
		Calendar c = Calendar.getInstance();
		c.setTime(ld);
		assertEquals(01, c.get(Calendar.DAY_OF_MONTH));
		assertEquals(00, c.get(Calendar.MONTH));
		assertEquals(2016, c.get(Calendar.YEAR));

	}
}
