package py.com.interacciones.interacciones;

import javax.annotation.PostConstruct;
import javax.ejb.Startup;
import javax.inject.Inject;

import com.google.inject.Singleton;

import lombok.Getter;
import py.com.interacciones.interacciones.logic.PermisoLogic;

@Singleton
@Startup
public class BeanProvider {

	public static BeanProvider INSTANCE;

	@Getter
	@Inject
	PermisoLogic logic;

	@PostConstruct
	public void init() {
		INSTANCE = this;
	}

}
