INSERT INTO paciente (ci, nombre, id) values ('41241'  , 'Juan Perez' , 1);
INSERT INTO paciente (ci, nombre, id) values ('4124231', 'Marta Chenu', 2);
INSERT INTO medico (apellidos, direccion, nombres, registro, ruc, telefono, id) values ('Vazquez' , 'En su casa', 'Eva'   , '123', '3333-5', '1212311', 1);
INSERT INTO medico (apellidos, direccion, nombres, registro, ruc, telefono, id) values ('Sandrito', 'En su casa', 'Sandro', '123', '1233-4', '1212311', 2);
INSERT INTO seguro (codigo, nombre, id) values ('ASI', 'ASISMED'    , 1);
INSERT INTO seguro (codigo, nombre, id) values ('SCL', 'SANTA CLARA', 2);
INSERT INTO contrato (codigo, numero_beneficiario, paciente_id, seguro_id, id) values ('66666', '1', 1, 2, 1);
INSERT INTO contrato (codigo, numero_beneficiario, paciente_id, seguro_id, id) values ('12314', '2', 2, 1, 2);