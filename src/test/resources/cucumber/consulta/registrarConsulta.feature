Feature: Registrar consulta 

Scenario: Agregar Consulta 

	Given Entro a la página 'views/consultas/registrar.xhtml'
	
	And Selecciono doctor 'Manuel Putito'
	And Paciente 'Putito'
	
	When Registro la consulta
	
	Then La lista de consultas de hoy tiene que ser
		| medico        | paciente | seguro  | horario |
		| Manuel Putito | putito   | ASISMED | 13:00   |
	
