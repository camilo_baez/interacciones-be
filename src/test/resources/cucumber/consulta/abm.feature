Feature: ABM de consultas 

Scenario: Agregar consulta 

	Given Entro en la página agregar consulta 
	And Selecciono el paciente con nombre 'Juan Perez' 
	And Selecciono el medico con nombre 'Eva' 
	
	When Presiono enter 
	
	Then La consulta visualizada debe ser: 
		| medico	  | paciente    | contrato | seguro     |
		| Eva Vazquez | Juan Perez  | 6666     | ASISMED    |
		
#Scenario: Explotacion
#	Given Entro en la página agregar consulta
#	And Selecciono el seguro con nombre 'ASISMED' 
#	And Selecciono el contrato con codigo '6666' y numero de beneficiario '3'