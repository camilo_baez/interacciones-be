Feature: Roles 

Scenario: Crear Rol 1 

	Given Visito la página 'views/rol/abm.xhtml' 
	And inserto el rol 
		| nombre        | descripcion       | 
		| rol 1         | descripcion 1     |
	And con los permisos 
		| descripcion   |
		| Descripcion 1 |
		| Descripcion 6 |
		
	When  presiono enter 
	
	Then La lista de roles debe ser: 
		| nombre        | descripcion       | 
		| rol 1         | descripcion 1     |
	
Scenario: Crear Rol 2

	Given Visito la página 'views/rol/abm.xhtml' 
	And inserto el rol 
		| nombre        | descripcion       | 
		| rol 2         | descripcion 2     |
	And con los permisos 
		| descripcion   |
		| Descripcion 2 |
		| Descripcion 5 |
		
	When  presiono enter 
	
	Then La lista de roles debe ser: 
		| nombre        | descripcion       | 
		| rol 1         | descripcion 1     |
		| rol 2         | descripcion 2     |