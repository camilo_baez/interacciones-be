Feature: ABM de clientes 

Scenario: crear cliente 

	Given entro en la página agregar cliente 
	
	
	And Inserto el cliente: 
		| razonSocial | ruc   |
		| Juan        | Perez |
		
	When presiono enter 
	
	Then La lista de clientes debe ser: 
		| razonSocial | ruc   |
		| Juan        | Perez |
		
		
Scenario: crear cliente 2 

	Given entro en la página agregar cliente 
	
	
	And Inserto el cliente: 
		| razonSocial | ruc    |
		| Pedro       | Torres |
		
	When presiono enter 
	
	Then La lista de clientes debe ser: 
		| razonSocial | ruc    |
		| Juan        | Perez  |
		| Pedro       | Torres |
		
Scenario: bucar cliente 
	Given entro en el listado de clientes 
	And Presiono el boton buscar en la grilla
	And Busco el cliente: 
		| razonSocial | ruc |
		| Pe          |     |
	When Presiono buscar 
	Then La lista de clientes debe ser: 
		| razonSocial | ruc    |
		| Pedro       | Torres |
		
	When Presiono limpiar
	Then La lista de clientes debe ser: 
		| razonSocial | ruc    |
		| Juan        | Perez  |
		| Pedro       | Torres |
	
		
		
Scenario: bucar cliente por ruc 
	Given entro en el listado de clientes 
	And Presiono el boton buscar en la grilla
	And Busco el cliente: 
		| razonSocial | ruc |
		|             | ere |
	When Presiono buscar 
	Then La lista de clientes debe ser: 
		| razonSocial | ruc   |
		| Juan        | Perez |

	When Presiono limpiar
	Then La lista de clientes debe ser: 
		| razonSocial | ruc    |
		| Juan        | Perez  |
		| Pedro       | Torres |
