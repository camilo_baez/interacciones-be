Feature: ABM de especialidad 

Scenario: crear especialidad

	Given entro en la página agregar especialidad
	And Inserto la especialidad: 
		| descripcion |
		| PEDIATRIA   |
	When presiono enter 
	Then La lista de especialidades debe ser: 
		| descripcion |
		| PEDIATRIA   |
		
		
Scenario: crear especialidad 2

	Given entro en la página agregar especialidad
	And Inserto la especialidad: 
		| descripcion |
		| OTORRINO    |
	When presiono enter 
	Then La lista de especialidades debe ser: 
		| descripcion |
		| PEDIATRIA   |
		| OTORRINO    |	
		
Scenario: bucar especialidad 
	
	Given entro en el listado de especialidades 
	And Presiono el boton buscar en la grilla
	And Busco la especialidad: 
		| descripcion |
		| OTO    |
	When Presiono buscar 
	Then La lista de especialidades debe ser: 
		| descripcion |
		| OTORRINO    |		
	When Presiono limpiar
	Then La lista de especialidades debe ser: 
		| descripcion |
		| PEDIATRIA   |
		| OTORRINO    |