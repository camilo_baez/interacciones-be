Feature: ABM de seguros 

Scenario: crear seguro 

	Given entro en la página agregar seguro 
	And Inserto el seguro: 
		| nombre      | direccion   | telefono  | ruc      |
		| Juan        | Overava 347 |021-666178 | 122112-0 |
	When presiono enter 
	Then La lista de seguros debe ser: 
		| nombre      | direccion   | telefono  | ruc      |
		| Juan        | Overava 347 |021-666178 | 122112-0 |
		
Scenario: crear seguro 2

	Given entro en la página agregar seguro
	And Inserto el seguro: 
		| nombre      | direccion   | telefono  | ruc   |
		| ASISMED     | Mcl Lopez 1 | 02122341  | 666-6 |		
	When presiono enter 	
	Then La lista de seguros debe ser: 
		| nombre      | direccion   | telefono   | ruc      |
		| Juan        | Overava 347 | 021-666178 | 122112-0 |
		| ASISMED     | Mcl Lopez 1 | 02122341   | 666-6    |
		
Scenario: editar seguro

	Given entro en la página listar seguros
	And Edito el seguro nro 2
	And Inserto el seguro: 
		| nombre        | direccion   | telefono  | ruc   |
		| ASISMED 2     | Mcl Lopez 1 | 02122341  | 666-6 |
	When presiono enter 
	Then La lista de seguros debe ser: 
		| nombre      | direccion   | telefono   | ruc      |
		| Juan        | Overava 347 | 021-666178 | 122112-0 |
		| ASISMED 2   | Mcl Lopez 1 | 02122341   | 666-6    |