Feature: ABM de Medicos 

Scenario: Agregar medico 
	Given Visito la página 'views/medico/abm.xhtml' 
	And  Inserto nombre 'Juan Andres', apellido 'Perez Gonzalez' 
	And  cuya dirección es 'Mcal Lopez', su telefono es '0972-105222', 
	And  registro '43597' y ruc '333333-0' 
	
	When  presiono enter 
	
	Then  La lista de medicos debe ser: 
		| nombres       | apellidos         | direccion  | registro | ruc      |
		| Juan Andres   | Perez Gonzalez    | Mcal Lopez | 43597    | 333333-0 |
		
Scenario: Agregar otro medico 
	Given Visito la página 'views/medico/abm.xhtml' 
	And  Inserto nombre 'Pedro Torres', apellido 'Perez Gonzalez' 
	And  cuya dirección es 'Mcal Lopez', su telefono es '0972-105222', 
	And  registro '43597' y ruc '333333-0' 
	
	When  presiono enter 
	
	Then  La lista de medicos debe ser: 
		| nombres       | apellidos         | direccion  | registro | ruc      |
		| Juan Andres   | Perez Gonzalez    | Mcal Lopez | 43597    | 333333-0 |
		| Pedro Torres  | Perez Gonzalez    | Mcal Lopez | 43597    | 333333-0 |
		
Scenario: Buscar medico 
	Given Visito la página 'views/medico/list.xhtml' 
	And  Presiono el boton buscar en la grilla 
	And  Inserto nombre 'Pedro Torres', apellido 'Perez Gonzalez' 
	
	When Presiono buscar 
	
	Then  La lista de medicos debe ser: 
		| nombres       | apellidos         | direccion  | registro | ruc      |
		| Pedro Torres  | Perez Gonzalez    | Mcal Lopez | 43597    | 333333-0 |
		
	When Presiono limpiar 
	
	Then  La lista de medicos debe ser: 
		| nombres       | apellidos         | direccion  | registro | ruc      |
		| Juan Andres   | Perez Gonzalez    | Mcal Lopez | 43597    | 333333-0 |
		| Pedro Torres  | Perez Gonzalez    | Mcal Lopez | 43597    | 333333-0 |
		
Scenario: Edito un medico 
	Given Visito la página 'views/medico/list.xhtml' 
	And  Entro en la pagina del medico número 2 
	And  Inserto nombre 'Adrian Tolomeo', apellido 'Perez Tardon' 
	
	When  presiono enter 
	
	Then  La lista de medicos debe ser: 
		| nombres         | apellidos         | direccion  | registro | ruc      |
		| Juan Andres     | Perez Gonzalez    | Mcal Lopez | 43597    | 333333-0 |
		| Adrian Tolomeo  | Perez Tardon      | Mcal Lopez | 43597    | 333333-0 |
		
Scenario: Eliminar medico 
	Given Visito la página 'views/medico/list.xhtml' 
	
	When Selecciono para eliminar al medico 2 
	And Presiono Borrar 
	
	Then  La lista de medicos debe ser: 
		| nombres         | apellidos         | direccion  | registro | ruc      |
		| Juan Andres     | Perez Gonzalez    | Mcal Lopez | 43597    | 333333-0 |
		
		
Scenario: Eliminar medico 
	Given Visito la página 'views/medico/list.xhtml' 
	
	When Selecciono para eliminar al medico 1 
	And Presiono Borrar 
	
	Then  La lista de medicos debe ser: 
		| nombres         | apellidos         | direccion  | registro | ruc      |
	