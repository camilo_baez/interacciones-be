Feature: Permisos 

Scenario: Agregar permiso 
	Given Visito la página 'views/permiso/abm.xhtml' 
	And Ingreso permiso: 
		| nombre        | descripcion       |
		| Permiso 1     | Descripcion 1     |
		
	When  presiono enter 
	
	Then  La lista de permisos debe ser: 
		| nombre        | descripcion       |
		| Permiso 1     | Descripcion 1     |
		
Scenario: Agregar permiso 
	Given Visito la página 'views/permiso/abm.xhtml' 
	And Ingreso permiso: 
		| nombre        | descripcion       |
		| Permiso 2     | Permiso nro 2     |
		
	When  presiono enter 
	
	Then  La lista de permisos debe ser: 
		| nombre        | descripcion       |
		| Permiso 1     | Descripcion 1     |
		| Permiso 2     | Permiso nro 2     |
		
Scenario: Editar permiso 
	Given Visito la página 'views/permiso/list.xhtml' 
	And Editor el permiso numero 2 
	And Ingreso permiso: 
		| nombre        | descripcion       |
		| Permiso 5     | Permiso nro 5     |
		
	When  presiono enter 
	
	Then  La lista de permisos debe ser: 
		| nombre        | descripcion       |
		| Permiso 1     | Descripcion 1     |
		| Permiso 5     | Permiso nro 5     |
		
Scenario: Buscar permiso 
	Given Visito la página 'views/permiso/list.xhtml' 
	And Presiono el boton buscar en la grilla 
	And Ingreso permiso: 
		| nombre        | descripcion       |
		| 1             | Descripcion       |
		
	When Presiono buscar 
	
	Then  La lista de permisos debe ser: 
		| nombre        | descripcion       |
		| Permiso 1     | Descripcion 1     |
		
Scenario: Eliminar Permiso 
	Given Visito la página 'views/permiso/list.xhtml' 
	And Presiono el boton eliminar el permiso 1 
	And Presiono Borrar 
	Then La lista de permisos debe ser: 
		| nombre        | descripcion       |
		| Permiso 5     | Permiso nro 5     |


Scenario: Eliminar Permiso 
	Given Visito la página 'views/permiso/list.xhtml' 
	And Presiono el boton eliminar el permiso 1 
	And Presiono Borrar 
	Then La lista de permisos debe ser: 
		| nombre        | descripcion       |