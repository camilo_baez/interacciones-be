Feature: Usuarios

Scenario: Crear user 1

	Given Visito la página 'faces/views/user/abm.xhtml'
	And inserto el usuario
		| nombre     | email            | telefono  | contrasenha |
		| Juan Perez | jperez@gmail.com | 123123123 | 123123123   |
	And con los roles
		| nombre |
		| Rol 1  |

	When  presiono enter

	Then La lista de usuarios debe ser:
		| nombre     | email            | telefono  |
		| Juan Perez | jperez@gmail.com | 123123123 |

Scenario: Crear user 2

	Given Visito la página 'faces/views/user/abm.xhtml'
	And inserto el usuario
		| nombre       | email           | telefono  | contrasenha |
		| Pedro Osorio | perez@gmail.com | 223223223 | 223223223   |
	And con los roles
		| nombre |
		| Rol 2  |
		| Rol 3  |

	When  presiono enter

	Then La lista de usuarios debe ser:
		| nombre	   | email            | telefono  |
		| Juan Perez   | jperez@gmail.com | 123123123 |
		| Pedro Osorio | perez@gmail.com  | 223223223 |

