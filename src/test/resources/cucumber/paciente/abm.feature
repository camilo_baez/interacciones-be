Feature: ABM de pacientes 

Scenario: crear paciente

	Given entro en la página agregar paciente
	And Inserto el paciente: 
		| nombre | apellido | cedula  | ruc       | domicilio                      | telefono   | celular    | nacionalidad | sexo      | genero       | estadoCivil | fechaNacimiento | email              |
		| Laura  | Martino  | 875998  | 875998-0  | Mcal. Lopez esq/ San Martin 22 | 021-559426 | 0983243566 | Paraguayo    | Femenino  | Heterosexual | Soltero     | 1990-08-08      | lamartino@yahoo.es |
	When presiono enter 
	Then La lista de pacientes debe ser: 
		| nombre | apellido | cedula  | ruc       | domicilio                      | telefono   | celular    | nacionalidad | sexo      | genero       | estadoCivil | fechaNacimiento | email              |
		| Laura  | Martino  | 875998  | 875998-0  | Mcal. Lopez esq/ San Martin 22 | 021-559426 | 0983243566 | Paraguayo    | Femenino  | Heterosexual | Soltero     | 1990-08-08      | lamartino@yahoo.es |
		
		
Scenario: crear paciente 2 

	Given entro en la página agregar paciente
	And Inserto el paciente: 
		| nombre | apellido | cedula  | ruc       | domicilio                      | telefono   | celular    | nacionalidad | sexo      | genero       | estadoCivil | fechaNacimiento | email              |
		| Pablo  | Zeballos | 1979002 | 1979002-0 | Eusebio Ayala 1422             | 021-664423 | 0985114787 | Paraguayo    | Masculino | Heterosexual | Soltero     | 1987-06-23      | pz10@hotmail.com   |
	When presiono enter 
	Then La lista de pacientes debe ser: 
		| nombre | apellido | cedula  | ruc       | domicilio                      | telefono   | celular    | nacionalidad | sexo      | genero       | estadoCivil | fechaNacimiento | email              |
		| Laura  | Martino  | 875998  | 875998-0  | Mcal. Lopez esq/ San Martin 22 | 021-559426 | 0983243566 | Paraguayo    | Femenino  | Heterosexual | Soltero     | 1990-08-08      | lamartino@yahoo.es |
		| Pablo  | Zeballos | 1979002 | 1979002-0 | Eusebio Ayala 1422             | 021-664423 | 0985114787 | Paraguayo    | Masculino | Heterosexual | Soltero     | 1987-06-23      | pz10@hotmail.com   |
		
		
Scenario: bucar paciente por nombre y apellido 
	
	Given entro en el listado de pacientes 
	And Presiono el boton buscar en la grilla
	And Busco el paciente: 
		| nombre | apellido | cedula  | ruc       | domicilio                      | telefono   | celular    | nacionalidad | sexo      | genero       | estadoCivil | fechaNacimiento | email              |
		| Pab    | Zeballos |         |           |                                |            |            |              |           |              |             |                 |                    |
	When Presiono buscar 
	Then La lista de pacientes debe ser: 
		| nombre | apellido | cedula  | ruc       | domicilio                      | telefono   | celular    | nacionalidad | sexo      | genero       | estadoCivil | fechaNacimiento | email              |
		| Pablo  | Zeballos | 1979002 | 1979002-0 | Eusebio Ayala 1422             | 021-664423 | 0985114787 | Paraguayo    | Masculino | Heterosexual | Soltero     | 1987-06-23      | pz10@hotmail.com   |		
	When Presiono limpiar
	Then La lista de pacientes debe ser: 
		| nombre | apellido | cedula  | ruc       | domicilio                      | telefono   | celular    | nacionalidad | sexo      | genero       | estadoCivil | fechaNacimiento | email              |
		| Laura  | Martino  | 875998  | 875998-0  | Mcal. Lopez esq/ San Martin 22 | 021-559426 | 0983243566 | Paraguayo    | Femenino  | Heterosexual | Soltero     | 1990-08-08      | lamartino@yahoo.es |
		| Pablo  | Zeballos | 1979002 | 1979002-0 | Eusebio Ayala 1422             | 021-664423 | 0985114787 | Paraguayo    | Masculino | Heterosexual | Soltero     | 1987-06-23      | pz10@hotmail.com   |
	
		
		
Scenario: bucar paciente por cedula 
	
	Given entro en el listado de pacientes 
	And Presiono el boton buscar en la grilla
	And Busco el paciente: 
		| nombre | apellido | cedula  | ruc       | domicilio                      | telefono   | celular    | nacionalidad | sexo      | genero       | estadoCivil | fechaNacimiento | email              |
		|        |          | 875998  |           |                                |            |            |              |           |              |             |                 |                    |
	When Presiono buscar 
	Then La lista de pacientes debe ser: 
		| nombre | apellido | cedula  | ruc       | domicilio                      | telefono   | celular    | nacionalidad | sexo      | genero       | estadoCivil | fechaNacimiento | email              |
		| Laura  | Martino  | 875998  | 875998-0  | Mcal. Lopez esq/ San Martin 22 | 021-559426 | 0983243566 | Paraguayo    | Femenino  | Heterosexual | Soltero     | 1990-08-08      | lamartino@yahoo.es |		
	When Presiono limpiar
	Then La lista de pacientes debe ser: 
		| nombre | apellido | cedula  | ruc       | domicilio                      | telefono   | celular    | nacionalidad | sexo      | genero       | estadoCivil | fechaNacimiento | email              |
		| Laura  | Martino  | 875998  | 875998-0  | Mcal. Lopez esq/ San Martin 22 | 021-559426 | 0983243566 | Paraguayo    | Femenino  | Heterosexual | Soltero     | 1990-08-08      | lamartino@yahoo.es |
		| Pablo  | Zeballos | 1979002 | 1979002-0 | Eusebio Ayala 1422             | 021-664423 | 0985114787 | Paraguayo    | Masculino | Heterosexual | Soltero     | 1987-06-23      | pz10@hotmail.com   |
